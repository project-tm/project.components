<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
if (Bitrix\Main\Loader::includeModule('igromafia.game')) {
    foreach ($arResult['ITEMS'] as &$item) {
        $item['PRICE'] = (isset($item['PROPERTIES']['PRICE']['VALUE']) ? $item['PROPERTIES']['PRICE']['VALUE'] : NULL);
        $item['EXCHANGE'] = (isset($item['PROPERTIES']['EXCHANGE']['VALUE_XML_ID']) && $item['PROPERTIES']['EXCHANGE']['VALUE_XML_ID'] == "Y");
        $item['PLATFORM'] = array();
        if (!empty($item['PROPERTIES']['PLATFORM']['VALUE'])) {
            foreach ((array) $item['DISPLAY_PROPERTIES']['PLATFORM']['VALUE'] as $key => $value) {
                $name = Igromafia\Game\Property::getPlatform($value);
                if ($name) {
                    $item['PLATFORM'][$key] = $name;
                }
            }
        }

        if (!empty($item['DETAIL_PICTURE'])) {
            $item['PICTURE'] = Project\Core\Image::resize($item['DETAIL_PICTURE']['ID'], 194, 218);
        } else {
            $item['GAME'] = Igromafia\Game\Lavka::getGame($item['NAME'], false);
            if ($item['GAME']['DETAIL_PICTURE']) {
                $item['PICTURE'] = Project\Core\Image::resize($item['GAME']['DETAIL_PICTURE'], 194, 218);
            } else {
                $item['PICTURE'] = $this->GetFolder() . "/img/noimg.jpg";
            }
        }
    }
}
