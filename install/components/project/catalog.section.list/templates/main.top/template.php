<?
if (empty($arResult['SECTION'])) {
    return;
}
?>
<div class="categories">
    <div class="categories__wrapper container">
        <h2 class="h2 _white"><span>Категории </span>товаров</h2>
        <div class="categories__inner">
            <? foreach ($arResult['SECTION'] as $arItem) { ?>
                <a class="categories__item _border" href="<?= $arItem['SECTION_PAGE_URL'] ?>">
                    <figure>
                        <div class="categories__img"><img src="<?= $arItem['PICTURE'] ?>" alt=""/></div>
                        <figcaption class="categories__caption"><span><?= $arItem['NAME'] ?></span></figcaption>
                    </figure>
                </a>
            <? } ?>
        </div>
    </div>
</div>