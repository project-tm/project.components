<?
if (empty($arResult['SECTION'])) {
    return;
}
?>
<div class="categories _white">
    <div class="categories__wrapper">
        <div class="categories__inner">
            <? foreach ($arResult['SECTION'] as $arItem) { ?>
                <a class="categories__item" href="<?= $arItem['SECTION_PAGE_URL'] ?>">
                    <figure>
                        <div class="categories__img"><img src="<?= $arItem['PICTURE'] ?>" alt=""></div>
                        <figcaption class="categories__caption"><span><?= $arItem['NAME'] ?></span></figcaption>
                    </figure>
                </a>
            <? } ?>
        </div>
    </div>
</div>