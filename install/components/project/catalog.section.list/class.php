<?php

use Bitrix\Main\Loader,
    Bitrix\Iblock;

class projectCatalogSectionList extends CBitrixComponent {

    public function executeComponent() {
        if (empty($this->arParams["CACHE_TIME"])) {
            $this->arParams["CACHE_TIME"] = 36000000;
        }

        $this->arParams["IBLOCK_TYPE"] = trim($this->arParams["IBLOCK_TYPE"]);
        if (empty($this->arParams["IBLOCK_TYPE"])) {
            $this->arParams["IBLOCK_TYPE"] = "catalog";
        }
        $this->arParams["IBLOCK_ID"] = (int) $this->arParams["IBLOCK_ID"];
        $this->arParams['FILTER_NAME'] = $this->arParams['FILTER_NAME'] ?: false;
        $this->arParams["PARENT_SECTION_ID"] = (int) $this->arParams["PARENT_SECTION_ID"];
        $this->arParams["PARENT_SECTION_CODE"] = $this->arParams["PARENT_SECTION_CODE"] ?: '';
        $this->arParams["INCLUDE_SUBSECTIONS"] = $this->arParams["INCLUDE_SUBSECTIONS"] != "N";

        $this->arParams["SORT_BY1"] = trim($this->arParams["SORT_BY1"]);
        if (empty($this->arParams["SORT_BY1"])) {
            $this->arParams["SORT_BY1"] = "ACTIVE_FROM";
        }
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams["SORT_ORDER1"])) {
            $this->arParams["SORT_ORDER1"] = "DESC";
        }
        if (empty($this->arParams["SORT_BY2"])) {
            $this->arParams["SORT_BY2"] = "SORT";
        }
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams["SORT_ORDER2"])) {
            $this->arParams["SORT_ORDER2"] = "ASC";
        }

        if ($this->startResultCache(false, array($this->arParams))) {
            if (Loader::includeModule('iblock')) {

                $arFilter = Array(
                    "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                    "ACTIVE" => "Y",
                );
                if ($this->arParams['FILTER_NAME']) {
                    foreach ($this->arParams['FILTER_NAME'] as $key => $value) {
                        if (empty($value)) {
                            unset($this->arParams['FILTER_NAME'][$key]);
                        }
                    }
                }
                if ($this->arParams['FILTER_NAME']) {
                    $arFilter['NAME'] = $this->arParams['FILTER_NAME'];
                    $arSort = array();
                } else {
                    $this->arParams["PARENT_SECTION_ID"] = CIBlockFindTools::GetSectionID(
                                    $this->arParams["PARENT_SECTION_ID"],
                                    $this->arParams["PARENT_SECTION_CODE"],
                                    array(
                                        "GLOBAL_ACTIVE" => "Y",
                                        "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                                    )
                    );
                    $arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION_ID'];
                    if ($this->arParams["INCLUDE_SUBSECTIONS"]) {
                        $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
                    }
                    $arSort = array(
                        $this->arParams["SORT_BY1"] => $this->arParams["SORT_ORDER1"],
                        $this->arParams["SORT_BY2"] => $this->arParams["SORT_ORDER2"],
                    );
                }
                $arSelect = array("ID", "NAME", 'PICTURE', 'SECTION_PAGE_URL');

                $res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
                $key = $this->arParams['FILTER_NAME'] ? 'NAME' : 'ID';
                while ($arItem = $res->GetNext()) {
                    $this->arResult['SECTION'][$arItem[$key]] = $arItem;
                }
                if ($this->arParams['FILTER_NAME']) {
                    $arData = array();
                    foreach ($this->arParams['FILTER_NAME'] as $value) {
                        if (isset($this->arResult['SECTION'][$value])) {
                            $arData[$value] = $this->arResult['SECTION'][$value];
                        }
                    }
                    $this->arResult['SECTION'] = $arData;
                }
                if ($this->arResult['SECTION']) {
                    $this->arResult['COUNT'] = count($this->arResult['SECTION']);
                    $this->setResultCacheKeys(array(
                        'COUNT'
                    ));
                }
                $this->includeComponentTemplate();
            }
        }
        return $this->arResult['COUNT'] ?: 0;
    }

}
