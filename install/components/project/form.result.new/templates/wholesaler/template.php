<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
?>
<? if ($arParams['WRAPPER']['IS_FULL']) { ?>
    <div class="contentText <?= $arParams['WRAPPER']["CONTANER"] ?>">
    <? } ?>
    <div class="contentContainer">
        <? if ($arResult["isFormErrors"] == "Y"): ?><div class="submit_message warning" style="opacity: 1;"><?= str_replace('Не заполнены следующие обязательные поля:', 'Заполните поле:', $arResult["FORM_ERRORS_TEXT"]); ?></div><? endif; ?>
        <? if ($arResult["FORM_RESULT"]) { ?>
            <div class="success"><?= $arResult["FORM_NOTE"] ?></div>
        <? } else { ?>
            <?= $arResult["FORM_NOTE"] ?>
        <? } ?>
        <?
        if ($arResult["isFormNote"] != "Y") {
            ?>
            <?= str_replace('method="POST"', 'method="POST" class="feedbackForm ' . $arParams['WRAPPER']["CONTANER_FORM"] . '"', $arResult["FORM_HEADER"]) ?>

            <div class="clear">
            </div>
            <div class="left">
                <p>
                    <label for="form_text_1" class="required">Представьтесь, пожалуйста<em>*</em></label>
                </p>
                <span class="forname logoinput">&nbsp;</span>
                <input id="form_text_6" type="text" name="form_text_6" value="" maxlength="50" class="required">
                <p>
                    <label for="form_text_2" class="required">телефон<em>*</em></label>
                </p>
                <span class="foremail logoinput">&nbsp;</span>
                <input id="form_text_7" type="text" name="form_text_7" value="" maxlength="255" class="required">
                <p>
                    <label for="form_text_3" class="required">Электронная почта<em>*</em></label>
                </p>
                <span class="foremail logoinput">&nbsp;</span>
                <input id="form_text_8" type="email" name="form_text_8" value="" maxlength="255" class="required email input-text">
            </div>
            <div class="left">
                <p>
                    <label for="form_textarea_5" class="required">Текст сообщения<em>*</em></label>
                </p>
                <textarea id="form_textarea_10" rows="11" cols="80" name="form_textarea_10" class="required"></textarea>
            </div>
            <?
            if ($arResult["isUseCaptcha"] == "Y") {
                ?>
                <div>
                    <p>
                        <label for="feedback_email" class="required"><?= GetMessage("FORM_CAPTCHA_TABLE_TITLE") ?><em>*</em></label>
                    </p>
                </div>
                <div>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>" class="required email input-text"/>
                </div>
                <div>
                    <span class="foremail logoinput">&nbsp;</span>
                    <input type="hidden" name="captcha_sid" value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
                    <input id="captcha_word" type="email" name="captcha_word" value="" maxlength="255" class="required email input-text">
                </div>
                <?
            } // isUseCaptcha
            ?>
            <div class="clear">
            </div>
            <div class="buttons">
                <div class="left">
                    <p class="back-link">
                        <input <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="hidden" name="web_form_submit" value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>" />
                        <input type="hidden" name="web_form_apply" value="Y" />
                        <input type="hidden" name="web_form_apply" value="<?= GetMessage("FORM_APPLY") ?>" />
                        <button type="submit" title="Отправить" class="buy_button <?= $arParams['WRAPPER']["CONTANER_FORM_BUTTON"] ?>">Отправить</button>
                    </p>
                </div>
                <div class="clear">
                </div>
            </div>
            <?
        } //endif (isFormNote)
        ?>
        </form>
    </div>
    <? if ($arParams['WRAPPER']['IS_FULL']) { ?>
    </div>
<? } ?>