<div class="static_part brands_list">
    <div class="h1">
        Наши бренды
    </div>
    <div class="fotorama custom_controls elements_rotator" data-width="940px" data-height="120px" data-loop="true" data-nav="false" data-transitionduration="500" data-click="false" data-autoplay="5000" data-arrows="false" data-swipe="false">
        <? foreach ($arResult['LIST'] as $list) { ?>
            <div class="slide">
                <? foreach ($list as $key=>$arItem) { ?>
                    <div class="element fotorama__select">
                        <a rel="nofollow" href="/search/?brand=<?=$key ?>"><img src="<?=$arItem['IMG'] ?>"></a>
                    </div>
                <? } ?>
            </div>
        <? } ?>
    </div>
    <div class="navigation static_part">
        <div class="left">
            <span class="sprite"></span>
        </div>
        <div class="right">
            <span class="sprite"></span>
        </div>
    </div>
</div>