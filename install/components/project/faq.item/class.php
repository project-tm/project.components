<?php

use Bitrix\Main\Loader;

class ProjectFaqItem extends CBitrixComponent {

    public function executeComponent() {
        if (Loader::includeModule('iblock') and Loader::includeModule('tm.opticsite')) {
            global $APPLICATION;
            $page = explode('/', $APPLICATION->GetCurPage(false));
            if (!empty($page[2])) {
                $arFilter = array(
                    'IBLOCK_ID' => Tm\Opticsite\Iblock\Config::FAQ,
                    'PROPERTY_OLD_NID' => $page[2]
                );
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, array('DETAIL_TEXT'));
                if ($arItem = $res->fetch()) {
                    echo $arItem['DETAIL_TEXT'];
                    exit;
                } else {
                    define('ERROR_404', 'Y');
                }
            }
        }
    }

}
