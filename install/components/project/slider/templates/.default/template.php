<div class="promo-block">
    <div class="promo-block__slider _slider bxslider">
        <? foreach ($arResult['SLIDER'] as $arItem) { ?>
            <div class="promo-block__slide" style="background-image: url(&quot;<?= $arItem['PREVIEW_PICTURE']['SRC'] ?>&quot;);">
                <div class="promo-block__slide-info">
                    <div class="container">
                        <div class="promo-block__title"><?= $arItem['NAME'] ?></div>
                        <div class="promo-block__desc"><?= $arItem['~PROPERTY_TEXT_VALUE'] ?></div>
                        <? if ($arItem['PROPERTY_LINK_VALUE']) { ?>
                            <a class="btn btn__more" href="<?= $arItem['PROPERTY_LINK_VALUE'] ?>"> <span>Подробнее</span></a>
                        <? } ?>
                    </div>
                </div>
            </div>
        <? } ?>
    </div>
</div>