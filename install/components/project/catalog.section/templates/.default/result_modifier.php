<?

preg_match('~<h1[^>]*>(.*)</h1>~imsU', $arResult['~UF_NOTICE'], $tmp);
if (!empty($tmp[1])) {
    $arResult['~UF_NOTICE'] = str_replace($tmp[0], '', $arResult['~UF_NOTICE']);
    $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] = $tmp[1];
}
if ($arResult['UF_BROWSER_TITLE']) {
    $arResult['IPROPERTY_VALUES']['SECTION_META_TITLE'] = $arResult['UF_BROWSER_TITLE'];
}
if ($arResult['UF_KEYWORDS']) {
    $arResult['IPROPERTY_VALUES']['SECTION_META_KEYWORDS'] = $arResult['UF_KEYWORDS'];
}
if ($arResult['UF_META_DESCRIPTION']) {
    $arResult['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION'] = $arResult['UF_META_DESCRIPTION'];
}