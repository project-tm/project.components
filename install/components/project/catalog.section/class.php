<?php

use Bitrix\Main\Loader,
    Bitrix\Iblock;

class projectCatalogSection extends CBitrixComponent {

    public function executeComponent() {
        if (empty($this->arParams["CACHE_TIME"])) {
            $this->arParams["CACHE_TIME"] = 36000000;
        }

        $this->arParams["IBLOCK_TYPE"] = trim($this->arParams["IBLOCK_TYPE"]);
        if (empty($this->arParams["IBLOCK_TYPE"])) {
            $this->arParams["IBLOCK_TYPE"] = "catalog";
        }
        $this->arParams["IBLOCK_ID"] = (int) $this->arParams["IBLOCK_ID"];
        $this->arParams["SECTION_ID"] = (int) $this->arParams["SECTION_ID"];
        $this->arParams["SECTION_CODE"] = $this->arParams["SECTION_CODE"] ?: '';

        if ($this->startResultCache(false, array($this->arParams))) {
            if (Loader::includeModule('iblock')) {

                $arFilter = Array(
                    "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                    "ACTIVE" => "Y",
                );

                $this->arParams["SECTION_ID"] = CIBlockFindTools::GetSectionID(
                                $this->arParams["SECTION_ID"],
                                $this->arParams["SECTION_CODE"],
                                array(
                                    "GLOBAL_ACTIVE" => "Y",
                                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                                )
                );
                if ($this->arParams["SECTION_ID"]) {
                    $arFilter['ID'] = $this->arParams["SECTION_ID"];
                    $arSelect = array("ID", "NAME", 'UF_NOTICE', 'DESCRIPTION', 'SECTION_PAGE_URL', 'UF_BROWSER_TITLE', 'UF_KEYWORDS', 'UF_META_DESCRIPTION');
                    $res = CIBlockSection::GetList($arSort, $arFilter, false, $arSelect);
                    if ($this->arResult = $res->GetNext()) {
                        $ipropValues = new Iblock\InheritedProperty\SectionValues($this->arParams['IBLOCK_ID'], $arItem['ID']);
                        $this->arResult['IPROPERTY_VALUES'] = $ipropValues->getValues();

                        $this->arResult['PATH'] = array();
                        $pathIterator = CIBlockSection::GetNavChain(
                                        $this->arResult['IBLOCK_ID'],
                                        $this->arResult['ID'],
                                        array(
                                            'ID', 'NAME', 'SECTION_PAGE_URL'
                                        )
                        );
                        $pathIterator->SetUrlTemplates('', $this->arParams['SECTION_URL']);
                        while ($path = $pathIterator->GetNext()) {
                            $ipropValues = new Iblock\InheritedProperty\SectionValues($this->arParams['IBLOCK_ID'], $path['ID']);
                            $path['IPROPERTY_VALUES'] = $ipropValues->getValues();
                            $this->arResult['PATH'][] = $path;
                        }

                        $this->setResultCacheKeys(array(
                            'NAME',
                            'PATH',
                            'ITEMS_TIMESTAMP_X',
                            'DESCRIPTION',
                            'IPROPERTY_VALUES',
                        ));

                        $this->includeComponentTemplate();
                    }
                }
            }
        }
        $this->initMetaData();
        return $this->arResult;
    }

    protected function initMetaData() {
        global $APPLICATION;
        if (empty($this->arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])) {
            $APPLICATION->SetTitle($this->arResult['NAME']);
        } else {
            $APPLICATION->SetTitle($this->arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']);
        }
        
        if (empty($this->arResult['IPROPERTY_VALUES']['SECTION_META_TITLE'])) {
            $APPLICATION->SetPageProperty('title', $this->arResult['NAME']);
        } else {
            $APPLICATION->SetPageProperty('title', $this->arResult['IPROPERTY_VALUES']['SECTION_META_TITLE']);
        }

        if ($this->arResult['IPROPERTY_VALUES']['SECTION_META_KEYWORDS']) {
            $APPLICATION->SetPageProperty('keywords', $this->arResult['IPROPERTY_VALUES']['SECTION_META_KEYWORDS']);
        }

        if ($this->arResult['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION']) {
            $APPLICATION->SetPageProperty('description', $this->arResult['IPROPERTY_VALUES']['SECTION_META_DESCRIPTION']);
        }

        if ($this->arResult['PATH']) {
            foreach ($this->arResult['PATH'] as $path) {
                if (empty($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'])) {
                    $APPLICATION->AddChainItem($path['NAME'], $path['~SECTION_PAGE_URL']);
                } else {
                    $APPLICATION->AddChainItem($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $path['~SECTION_PAGE_URL']);
                }
            }
        }
        $APPLICATION->AddChainItem($path['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'], $path['~SECTION_PAGE_URL']);

        if ($this->arResult['ITEMS_TIMESTAMP_X']) {
            Main\Context::getCurrent()->getResponse()->setLastModified($this->arResult['ITEMS_TIMESTAMP_X']);
        }
    }

}
