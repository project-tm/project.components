<?php

use Bitrix\Main\Loader,
    Bitrix\Iblock;

class projecNewsList extends CBitrixComponent {

    public function executeComponent() {
        if (empty($this->arParams["CACHE_TIME"])) {
            $this->arParams["CACHE_TIME"] = 36000000;
        }

        $this->arParams["IBLOCK_TYPE"] = trim($this->arParams["IBLOCK_TYPE"]);
        if (empty($this->arParams["IBLOCK_TYPE"])) {
            $this->arParams["IBLOCK_TYPE"] = "news";
        }
        $this->arParams["IBLOCK_ID"] = (int) $this->arParams["IBLOCK_ID"];
        $this->arParams["NEWS_COUNT"] = (int) $this->arParams["NEWS_COUNT"];
        if (empty($this->arParams["NEWS_COUNT"])) {
            $this->arParams["NEWS_COUNT"] = 20;
        }
        $this->arParams["SORT_BY1"] = trim($this->arParams["SORT_BY1"]);
        if (empty($this->arParams["SORT_BY1"])) {
            $this->arParams["SORT_BY1"] = "ACTIVE_FROM";
        }
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams["SORT_ORDER1"])) {
            $this->arParams["SORT_ORDER1"] = "DESC";
        }
        if (empty($this->arParams["SORT_BY2"])) {
            $this->arParams["SORT_BY2"] = "SORT";
        }
        if (!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $this->arParams["SORT_ORDER2"])) {
            $this->arParams["SORT_ORDER2"] = "ASC";
        }

        $this->arParams["PARENT_SECTION_ID"] = (int) $this->arParams["PARENT_SECTION_ID"];
        $this->arParams["PARENT_SECTION_CODE"] = $this->arParams["PARENT_SECTION_CODE"] ?: '';
        $this->arParams["INCLUDE_SUBSECTIONS"] = $this->arParams["INCLUDE_SUBSECTIONS"] != "N";

        $this->arParams["ACTIVE_DATE_FORMAT"] = trim($this->arParams["ACTIVE_DATE_FORMAT"]);
        if (empty($this->arParams["ACTIVE_DATE_FORMAT"])) {
            $this->arParams["ACTIVE_DATE_FORMAT"] = $DB->DateFormatToPHP(CSite::GetDateFormat("SHORT"));
        }

        if ($this->startResultCache(false, array($this->arParams))) {
            if (Loader::includeModule('iblock')) {

                $arSort = array(
                    $this->arParams["SORT_BY1"] => $this->arParams["SORT_ORDER1"],
                    $this->arParams["SORT_BY2"] => $this->arParams["SORT_ORDER2"],
                );

                $arFilter = Array(
                    "IBLOCK_TYPE" => $this->arParams['IBLOCK_TYPE'],
                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                    "ACTIVE" => "Y",
                );

                $this->arParams["PARENT_SECTION_ID"] = CIBlockFindTools::GetSectionID(
                                $this->arParams["PARENT_SECTION_ID"],
                                $this->arParams["PARENT_SECTION_CODE"],
                                array(
                                    "GLOBAL_ACTIVE" => "Y",
                                    "IBLOCK_ID" => $this->arParams['IBLOCK_ID'],
                                )
                );
                if ($this->arParams["PARENT_SECTION_ID"]) {
                    $arFilter['SECTION_ID'] = $this->arParams['PARENT_SECTION_ID'];
                }
                if ($this->arParams["INCLUDE_SUBSECTIONS"]) {
                    $arFilter['INCLUDE_SUBSECTIONS'] = 'Y';
                }

                if (!empty($this->arParams["FILTER_NAME"]) and ! preg_match("/^[a-z_][a-z0-9_]*$/i", $this->arParams["FILTER_NAME"])) {
                    $arrFilter = $GLOBALS[$this->arParams["FILTER_NAME"]];
                    if ($arrFilter) {
                        $arFilter = $arrFilter + $arrFilter;
                    }
                }

                $arSelect = array("ID", "NAME", 'ACTIVE_FROM', 'DETAIL_PAGE_URL');
                if (!empty($this->arParams['FIELD_CODE'])) {
                    foreach ($this->arParams['FIELD_CODE'] as $value) {
                        if ($value) {
                            $arSelect[] = $value;
                        }
                    }
                }
                if (!empty($this->arParams['PROPERTY_CODE'])) {
                    foreach ($this->arParams['PROPERTY_CODE'] as $value) {
                        if ($value) {
                            $arSelect[] = 'PROPERTY_' . $value;
                        }
                    }
                }

                $arNavParams = array(
                    "nTopCount" => $this->arParams["NEWS_COUNT"],
                );
                $arNavigation = false;
                $res = CIBlockElement::GetList($arSort, $arFilter, false, $arNavParams, array_unique($arSelect));
                $this->arResult['NEWS'] = array();
                while ($arItem = $res->GetNext()) {
                    if ($arItem["ACTIVE_FROM"]) {
                        $arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($this->arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
                    } else {
                        $arItem["DISPLAY_ACTIVE_FROM"] = "";
                    }
                    $this->arResult['NEWS'][] = $arItem;
                }

                $this->includeComponentTemplate();
            }
        }
    }

}
