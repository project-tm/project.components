<div class="news-block">
    <div class="news-block__title container">
        <h2 class="h2"><span>Наши </span>новости</h2>
    </div>
    <div class="news-block__articles">
        <? foreach ($arResult['NEWS'] as $arItem) { ?>
            <a class="news-block__item" href="<?=$arItem['DETAIL_PAGE_URL'] ?>">
                <figure class="news-block__item-figure">
                    <div class="news-block__item-img"><img src="<?=$arItem['PICTURE'] ?>" alt=""/></div>
                </figure>
                <div class="news-block__item-desc"><?=$arItem['NAME'] ?></div>
                <div class="news-block__item-date"><?=$arItem['DISPLAY_ACTIVE_FROM'] ?></div>
            </a>
        <? } ?>
    </div>
</div> 
