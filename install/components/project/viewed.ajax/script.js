(function (window) {
    'use strict';

    if (window.jsProjectViewedAjax) {
        return;
    }

    window.jsProjectViewedAjax = function (arParams) {
        $(function () {
            $.get(arParams.AJAX, {
                TYPE: arParams.TYPE,
                ELEMENT_ID: arParams.ELEMENT_ID
            });
        });
    };

})(window);