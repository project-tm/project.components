<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Page\Asset,
    Bitrix\Main\Application,
    Project\Core\Model\FavoritesTable;

CBitrixComponent::includeComponentClass("project:favorites.ajax");

class PortalFavoritesList extends ProjectFavorites {

    private function startWrapperTemplate() {
        Asset::getInstance()->addJs($this->arResult['SCRIPT']);
        $jsParams = array(
            'AJAX' => $this->arResult['AJAX'],
            'TEMPLATE_NAME' => $this->arResult['TEMPLATE_NAME'],
            "PAGEN" => $this->arResult["PAGEN"],
            "TYPE" => $this->arParams["TYPE"],
            'CONTANER' => '.' . $this->arResult['CONTANER'],
            'CONTANER_DELETE' => '.' . $this->arResult['CONTANER_DELETE'],
        );
        ?>
        <script>
            var <?= $this->arResult['JS_OBJECT'] ?> = new jsProjectFavoritesList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
        </script>
        <?
    }

    public function executeComponent() {
        if (Loader::includeModule('project.core') and CUser::IsAuthorized()) {
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';


            $userId = cUser::GetID();
            $request = Application::getInstance()->getContext()->getRequest();
            if ($this->arResult['IS_AJAX']) {
                if (!empty($this->arParams['DELETE']) and ! empty($this->arParams['ELEMENT_ID'])) {
                    $rsData = FavoritesTable::getList(array(
                                'select' => array('ID'),
                                'filter' => array(
                                    'TYPE' => $this->arParams['TYPE'],
                                    'USER_ID' => $userId,
                                    'ELEMENT_ID' => $this->arParams['ELEMENT_ID']
                                ),
                    ));
                    $rsData = new CDBResult($rsData);
                    if ($arItem = $rsData->Fetch()) {
                        FavoritesTable::Delete($arItem['ID']);
                    }
                }
            }

            $rsData = FavoritesTable::getList(array(
                        'select' => array('ELEMENT_ID'),
                        'filter' => array(
                            'TYPE' => $this->arParams['TYPE'],
                            'USER_ID' => $userId
                        ),
            ));
            $rsData = new CDBResult($rsData);
            $this->arResult['FILTER'] = array();
            while ($arItem = $rsData->Fetch()) {
                $this->arResult['FILTER'][] = $arItem['ELEMENT_ID'];
            }

            $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
            $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
            $this->arResult['PAGEN'] = ceil($this->arParams['PAGEN_1'] ?: ($request->get('PAGEN_1') ?: 1));
            $this->arResult['PARAM'] = $this->arParams['PARAM'] ?: '';
            $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
            $key = sha1($this->arResult['TEMPLATE_NAME'] . $this->arResult['PARAM'] ?: '');

            $this->arResult['CONTANER'] = 'ajax-favorites-list' . $key;
            $this->arResult['CONTANER_DELETE'] = 'ajax-favorites-list-delete' . $key;
            $this->arResult['JS_OBJECT'] = 'jsProjectFavoritesList' . $key;
            if (empty($this->arResult['IS_AJAX'])) {
                $this->startWrapperTemplate();
            }
            $this->includeComponentTemplate();
            return true;
        }
    }

}
