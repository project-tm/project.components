<?php

use Bitrix\Main\Loader;

class ProjectCatalogSiblings extends CBitrixComponent {

    public function executeComponent() {

        /* используем кеш */
        if (Bitrix\Main\Loader::includeModule('project.core')) {
            $arParams = $this->arParams;
            list($section_id, $element_id) = Project\Core\Utility::useCache(array(__CLASS__, $arParams), function() use($arParams) {
                /* устанавливаем section_id */
                $section_id = $arParams["IBLOCK_SECTION_ID"];
                if (empty($section_id) && !empty($arParams["SECTION_CODE"])){
                    $section_filter = array(
                        "CODE" =>      $arParams["SECTION_CODE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    );
                    $rsSection = CIBlockSection::GetList(array(), $section_filter, false, array("ID"));
                    while($arSection = $rsSection->Fetch()){
                        $section_id = $arSection["ID"];
                    }
                }

                /* устанавливаем element_id */
                $element_id = $arParams["ID"];
                if (empty($element_id) && !empty($arParams["ELEMENT_CODE"])){
                    $element_filter = array(
                        "CODE" =>      $arParams["ELEMENT_CODE"],
                        "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    );
                    $rsElement = CIBlockElement::GetList(array(), $element_filter, false, false, array("ID"));
                    while($arElement = $rsElement->Fetch()){
                        $element_id = $arElement["ID"];
                    }
                }

                return array($section_id, $element_id);
            });
        }

        /* сортировка и фильтр по умолчанию */
        $arNPSort = array(
            $this->arParams["ELEMENT_SORT_FIELD"] => $this->arParams["ELEMENT_SORT_ORDER"],
            $this->arParams["ELEMENT_SORT_FIELD2"] => $this->arParams["ELEMENT_SORT_ORDER2"],
        );
        $arNPFilter = array(
            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
            "SECTION_ID" => $section_id,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
        );

        /* start извлекаем сохраненные в сессии данные о выбранных фильтрах и сортировке в разделе */
        $for_product_siblings = $_SESSION["FOR_PRODUCT_SIBLINGS"][$section_id];
        if (!empty($for_product_siblings)){
            /* фильтр */
            if (!empty($for_product_siblings["FILTER"])){
                $arNPFilter = array_merge($arNPFilter, $for_product_siblings["FILTER"]);
            }
            /* сортировка */
            if (!empty($for_product_siblings["SORT"])){
                $arNPSort = array(
                    $for_product_siblings["SORT"]["SIBLINGS_SORT_FIELD"] => $for_product_siblings["SORT"]["SIBLINGS_SORT_ORDER"],
                    $for_product_siblings["SORT"]["SIBLINGS_SORT_FIELD2"] => $for_product_siblings["SORT"]["SIBLINGS_SORT_ORDER2"],
                );
            }
        }
        /* end извлекаем сохраненные в сессии данные о выбранных фильтра и сортировке в разделе */

        if ($this->startResultCache(false, array($this->arParams, $arNPFilter))) {
            if (Loader::includeModule('iblock')) {

                $arNPNavParams = array("nPageSize" => 1, "nElementID" => $element_id);
                $arNPSelect = array("ID", "NAME", "DETAIL_PAGE_URL", 'CATALOG_PRICE_1');
                $rsNPElement = CIBlockElement::GetList($arNPSort, $arNPFilter, false, $arNPNavParams, $arNPSelect);
                $isSearch = 0;
                $first = $last = 0;
                while ($arNPElement = $rsNPElement->GetNext()) {
                    if($isSearch) {
                        $this->arResult['SIBLINGS']['NEXT'] = $arNPElement;
                    } else {
                        if($element_id == $arNPElement['ID']) {
                            $isSearch = true;
                        } else {
                            $this->arResult['SIBLINGS']['PREV'] = $arNPElement;
                        }
                    }
                    if(empty($first)) {
                        $first = $arNPElement;
                    }
                    $last = $arNPElement;
                }
                if(empty($this->arResult['SIBLINGS']['PREV'])) {
                    $this->arResult['SIBLINGS']['PREV'] = $last;
                }
                if(empty($this->arResult['SIBLINGS']['NEXT'])) {
                    $this->arResult['SIBLINGS']['NEXT'] = $first;
                }

                $this->includeComponentTemplate();
            }
        }
    }
}