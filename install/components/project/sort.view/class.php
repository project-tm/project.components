<?php

use Bitrix\Main\Application;

class ProjectSortView extends CBitrixComponent {

    static private function initParam($name, $map, $default = '') {
        global $APPLICATION;
        $request = Application::getInstance()->getContext()->getRequest();
        $item = $request->get($name);
        if (empty($item) and ! empty($_COOKIE['sort-view-' . $name])) {
            $item = $_COOKIE['sort-view-' . $name];
        }
        if (empty($default)) {
            $default = key($map);
        }
        if (!isset($map[$item])) {
            $item = $default;
        }
        $map[$item]['select'] = true;
        foreach ($map as $key => &$value) {
            $value['url'] = $APPLICATION->GetCurPageParam(($name!='view' and $default == $key) ? '' : $name . '=' . $key, array($name, 'clear_cache', 'bitrix_include_areas'));
        }
        $map[$item]['key'] = strtoupper($item);
        return array($map[$item], $map);
    }

    public function executeComponent() {

        $arParamsView = array(
            'card' => array(),
            'line' => array()
        );
        $arParamsSort = array(
            'price-desc' => array(
                'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
                    'PROPERTY_MINIMUM_PRICE' => 'DESC',
                    'PROPERTY_MAXIMUM_PRICE' => 'DESC'
                ),
                'name' => 'цене: Дорогие — Дешевые'
            ),
            'price-asc' => array(
                'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
                    'PROPERTY_MINIMUM_PRICE' => 'ASC',
                    'PROPERTY_MAXIMUM_PRICE' => 'ASC'
                ),
                'name' => 'цене: Дешевые — Дорогие',
            ),
//            'img-on' => 'изображению: есть — нет',
//            'img-off' => 'изображению: нет — есть',
            'name-asc' => array(
                'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
                    'NAME' => 'ASC'
                ),
                'name' => 'названию: А — я'
            ),
            'name-desc' => array(
                'sort' => array(
//                    'CATALOG_AVAILABLE' => 'DESC',
                    'NAME' => 'DESC'
                ),
                'name' => 'названию: я — А'
            ),
            'availability-desc' => array(
                'sort' => array(
                    'CATALOG_QUANTITY' => 'DESC'
                ),
                'name' => 'наличию: много — мало'
            ),
            'availability-asc' => array(
                'sort' => array(
                    'CATALOG_QUANTITY' => 'ASC,NULLS'
                ),
                'name' => 'наличию: мало — много'
            ),
            'date-desc' => array(
                'sort' => array(
                    'CREATED' => 'ASC'
                ),
                'name' => 'дате: новые — старые'
            ),
            'date-asc' => array(
                'sort' => array(
                    'CREATED' => 'ASC'
                ),
                'name' => 'дате: старые — новые'
            ),
            'rating-asc' => array(
                'sort' => array(
                    'PROPERTY_RATING' => 'ASC,NULLS'
                ),
                'name' => 'рейтингу: Хорошие — Плохие'
            ),
            'rating-desc' => array(
                'sort' => array(
                    'PROPERTY_RATING' => 'DESC'
                ),
                'name' => 'рейтингу: Плохие — Хорошие'
            ),
            'comment-desc' => array(
                'sort' => array(
                    'PROPERTY_FORUM_MESSAGE_CNT' => 'DESC'
                ),
                'name' => 'количеству отзывов: Много — Мало'
            ),
            'comment-asc' => array(
                'sort' => array(
                    'PROPERTY_FORUM_MESSAGE_CNT' => 'ASC,NULLS'
                ),
                'name' => 'количеству отзывов: Мало — Много'
            )
        );
        if (isset($this->arParams['SORT'])) {
            $arParamsSort = array_merge(array(
                'search' => $this->arParams['SORT']
                    ), $arParamsSort);
        }
        list($this->arResult['itemView'], $this->arResult['arView']) = self::initParam('view', $arParamsView);
        list($this->arResult['itemSort'], $this->arResult['arSort']) = self::initParam('sort', $arParamsSort, 'availability-desc');
        $this->includeComponentTemplate();
        return array($this->arResult['itemView'], $this->arResult['itemSort']);
    }

}
