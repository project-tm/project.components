<div class="recently-viewed _category">
    <div class="recently-viewed__wrapper">
        <div class="container">
            <h2 class="h2"><span>Недавно </span>смотрели</h2>
        </div>
        <div class="product-slider _m0">
            <form class="product-slider__slide" action="">
                <figure class="product-slider__slide-figure">
                    <div class="product-slider__slide-img"><img src="<?= SITE_TEMPLATE_PATH ?>/build/assets/images/img/product-slide__img-1.png" alt=""/></div>
                </figure>
                <div class="product-slider__slide-desc">Алькантара Корея Decoin</div>
                <div class="product-slider__slide-price">549 руб.</div>
                <select name="">
                    <option value="1">Погонными метрами (1,52 х 1 м)</option>
                    <option value="2">Рулонами (1,52 х 15 м)</option>
                    <option value="3">Рулонами (1 х 12 м)</option>
                </select><a class="product-slider__slide-btn" href="#"><span>В корзину</span></a><a class="btn btn__more _catalog" href="#"> <span>Подробнее</span></a>
            </form>
            <form class="product-slider__slide" action="">
                <div class="product-slider__slide-labels">
                    <div class="product-slider__slide-hit"> <span>Хит</span></div>
                </div>
                <figure class="product-slider__slide-figure">
                    <div class="product-slider__slide-img"><img src="<?= SITE_TEMPLATE_PATH ?>/build/assets/images/img/product-slide__img-2.png" alt=""/></div>
                </figure>
                <div class="product-slider__slide-desc">Диодные лампы в головной свет</div>
                <div class="product-slider__slide-price">549 руб.</div>
                <select name="">
                    <option value="1">Погонными метрами (1,52 х 1 м)</option>
                    <option value="2">Рулонами (1,52 х 15 м)</option>
                    <option value="3">Рулонами (1 х 12 м)</option>
                </select><a class="product-slider__slide-btn" href="#"><span>В корзину</span></a><a class="btn btn__more _catalog" href="#"> <span>Подробнее</span></a>
            </form>
            <form class="product-slider__slide" action="">
                <figure class="product-slider__slide-figure">
                    <div class="product-slider__slide-img"><img src="<?= SITE_TEMPLATE_PATH ?>/build/assets/images/img/product-slide__img-3.png" alt=""/></div>
                </figure>
                <div class="product-slider__slide-desc">Ракель магнит + замша 3м.</div>
                <div class="product-slider__slide-price">389 руб.</div>
                <select name="">
                    <option value="1">Погонными метрами (1,52 х 1 м)</option>
                    <option value="2">Рулонами (1,52 х 15 м)</option>
                    <option value="3">Рулонами (1 х 12 м)</option>
                </select><a class="product-slider__slide-btn" href="#"><span>В корзину</span></a><a class="btn btn__more _catalog" href="#"> <span>Подробнее</span></a>
            </form>
            <form class="product-slider__slide" action="">
                <figure class="product-slider__slide-figure">
                    <div class="product-slider__slide-img"><img src="<?= SITE_TEMPLATE_PATH ?>/build/assets/images/img/product-slide__img-1.png" alt=""/></div>
                </figure>
                <div class="product-slider__slide-desc">Алькантара Корея Decoin</div>
                <div class="product-slider__slide-price">549 руб.</div>
                <select name="">
                    <option value="1">Погонными метрами (1,52 х 1 м)</option>
                    <option value="2">Рулонами (1,52 х 15 м)</option>
                    <option value="3">Рулонами (1 х 12 м)</option>
                </select><a class="product-slider__slide-btn" href="#"><span>В корзину</span></a><a class="btn btn__more _catalog" href="#"> <span>Подробнее</span></a>
            </form>
            <form class="product-slider__slide" action="">
                <figure class="product-slider__slide-figure">
                    <div class="product-slider__slide-img"><img src="<?= SITE_TEMPLATE_PATH ?>/build/assets/images/img/product-slide__img-1.png" alt=""/></div>
                </figure>
                <div class="product-slider__slide-desc">Алькантара Корея Decoin</div>
                <div class="product-slider__slide-price">549 руб.</div>
                <select name="">
                    <option value="1">Погонными метрами (1,52 х 1 м)</option>
                    <option value="2">Рулонами (1,52 х 15 м)</option>
                    <option value="3">Рулонами (1 х 12 м)</option>
                </select><a class="product-slider__slide-btn" href="#"><span>В корзину</span></a><a class="btn btn__more _catalog" href="#"> <span>Подробнее</span></a>
            </form>
        </div>
    </div>
</div>