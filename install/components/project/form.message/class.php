<?php

use Bitrix\Main\Loader,
    Bitrix\Main\Application,
    Project\Core\Form\Config;

CBitrixComponent::includeComponentClass("project:form");

class ProjectFormMessage extends ProjectForm {

    protected function getFormId() {
        throw Config::MESSAGE;
    }

    protected function filterForm(&$arData) {
        $request = Application::getInstance()->getContext()->getRequest();
        if (empty($request->getPost('MESSAGE'))) {
            $this->arResult['ERROR']['MESSAGE'] = 'Поле «Cообщение» не заполнено';
        } else {
            $arData['MESSAGE'] = $request->getPost('MESSAGE');
        }
    }

    protected function sendForm($arData) {
        $arData['SELLER_ID'] = $this->arParams['SELLER_ID'];
        $arData['GAME_ID'] = $this->arParams['GAME']['ID'];
        $arData['THEME'] = $this->arParams['GAME']['THEME'];
        return parent::sendForm($arData);
    }

}
