<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?
use Bitrix\Main\Localization\Loc;
use Citfact\Tools\Catalog\Services\CompareService;

CModule::includeModule("citfact.tools");
CModule::includeModule("citfact.socials.meta");

CJSCore::Init(array("fx"));
Loc::loadMessages(__FILE__);

(new \Dotenv\Dotenv($_SERVER["DOCUMENT_ROOT"]))->load();
?>
    <!DOCTYPE html>
<html lang="<?= LANGUAGE_ID ?>">
    <head>

        <title><? $APPLICATION->ShowTitle(); ?></title>
        <?
        $APPLICATION->ShowHead();

        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/build/build.css');
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/app/font.css');
        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/build/main.js');
		$APPLICATION->AddHeadScript('/js/goals.js');
        $APPLICATION->setPageProperty("og:image", SITE_TEMPLATE_PATH . "/images/logo.png");
        $APPLICATION->addHeadScript("https://api-maps.yandex.ru/2.1/?lang=ru_RU");

        \Citfact\Meta::showMetadata();

        ?>
        <script>
            window.INLINE_SVG_REVISION = <?= filemtime($_SERVER['DOCUMENT_ROOT'] . '/local/templates/frontmaster/build/svgSprite/sprite.svg') ?>
        </script>
        <meta name="viewport" content="width=device-width, maximum-scale=1.0, initial-scale=1.0, user-scalable=no">
        <link rel="apple-touch-icon" sizes="180x180" href="<?= SITE_TEMPLATE_PATH ?>/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="<?= SITE_TEMPLATE_PATH ?>/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?= SITE_TEMPLATE_PATH ?>/favicon-16x16.png" sizes="16x16">
        <link rel="manifest" href="<?= SITE_TEMPLATE_PATH ?>/manifest.json">
        <link rel="mask-icon" href="<?= SITE_TEMPLATE_PATH ?>/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

        <?
            if (!empty($_REQUEST["PAGEN_1"]) || !empty($_REQUEST["sort_by"]) || !empty($_REQUEST["typeView"])){
                $CURRENT_PAGE = "https://";//(CMain::IsHTTPS()) ? "https://" : "http://";
                $CURRENT_PAGE .= SITE_SERVER_NAME;
                $CURRENT_PAGE .= $APPLICATION->GetCurPage(false);
                ?><link rel="canonical" href="<?=$CURRENT_PAGE;?>"/><?
            }
        ?>

    </head>


<? \Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area_location_three");
global $cityFilter;
if (isset($_GET['city-filter'])) {
    global $cityFilter;
    $cityFilter['NAME'] = $_GET['city-filter'];
}
if (CModule::includeModule("citfact.location")) {
    $location = new \Citfact\Location\Location();
    $changeVar = \Citfact\Location\Location::CHANGE_VAR;

    if ($requestCity = $_REQUEST[$changeVar]) {
        $location->trySetCityById($requestCity);
    }

    global $locationID;
    global $locationName;
    global $locationOrderId;
    $locationID = $location->getObjField('ID');
    $locationName = $location->getObjField('NAME');
    $locationOrderId=$location->getObjProperty('ORDER_LOCATION_ID');
}
\Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area_location_three", "");



$mainClass = '';
$curDir = $APPLICATION->GetCurDir();
$is404 = defined('ERROR_404');

if ($curDir == '/' && !$is404) {
    $mainClass = 'main--index';
    $openDropdown = true;
} else if ($curDir == '/personal/order/make/') {
    $mainClass = 'main--checkout';
} else if (($curDir == '/register/') || ($curDir == '/auth/')) {
    $mainClass = 'main--bg main--reg';
} else if (defined("WHITE_SECTION")) {
    $mainClass = '';
} else {
    $mainClass = 'main--bg';
}

$APPLICATION->AddViewContent("mainClass", $mainClass);
?>

<body>
    <?if(getenv('ENVIROMENT') == "production") {?>
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-T7KGZN"
                          height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-T7KGZN');</script>
        <!-- End Google Tag Manager -->
    <?}?>
<? $APPLICATION->ShowPanel(); ?>
    <header>
        <div class="header">
            <div class="header-mobile">
                <div class="header-mobile__top">
                    <div class="container">
                        <div class="header-mobile__top-wrap">
                            <div class="header-mobile__burger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                            <a href="#modal-callback" class="header-mobile__icon" data-modal-open>
                                <svg class="i-icon">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                            </a>
                            <a href="/catalog/index.php?q=" class="header-mobile__icon">
                                <svg class="i-icon">
                                    <use xlink:href="#icon-zoom"></use>
                                </svg>
                            </a>
                            <? \Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area_enter");
                            global $USER;
                            if ($USER->isAuthorized()) { ?>
                                <a href="/personal/" class="header-mobile__icon">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-user_3"></use>
                                    </svg>
                                </a>
                            <? } else { ?>
                                <a href="/auth/" class="header-mobile__icon">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-user_3"></use>
                                    </svg>
                                </a>
                            <? } ?>
                            <? \Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area_enter", ""); ?>
                            <?
                            $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "", array(
                                    "HIDE_ON_BASKET_PAGES" => "Y",
                                    "PATH_TO_BASKET" => SITE_DIR . "personal/cart/",
                                    "PATH_TO_ORDER" => SITE_DIR . "personal/order/make/",
                                    "SHOW_DELAY" => "N",
                                    "SHOW_EMPTY_VALUES" => "Y",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PRICE" => "Y",
                                    "SHOW_SUMMARY" => "Y",
                                    "SHOW_TOTAL_PRICE" => "Y"
                                )
                            ); ?>

                        </div>
                    </div>
                </div>
                <div class="mobile-menu">
                    <div class="mobile-menu__top">
                        <div class="mobile-menu__label">
                            Меню
                        </div>
                        <div class="mobile-menu__close"></div>
                    </div>
                    <a class="mobile-menu__city" href="#modal-city" data-modal-open>
                        <svg class="i-icon">
                            <use xlink:href="#icon-geolocation"></use>
                        </svg>
                        <? \Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area_location"); ?>
                        <span>
                    <?= $locationName ?>
                        </span>
                        <? \Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area_location", "Загрузка..."); ?>
                    </a>
                    <a class="mobile-menu__ceo" href="#modal-boss" data-modal-open="">
                        <svg class="i-icon">
                            <use xlink:href="#icon-mail"></use>
                        </svg>
                        <span>Обратиться к руководителю</span>
                    </a>
                    <div class="mobile-menu__icons">
                        <? /*?>
                    <div class="mobile-menu__icon">
                        <a href="" class="mobile-menu__icon-link"></a>
                        <div class="mobile-menu__icon-left">
                            <div class="mobile-menu__icon-icon">
                                <svg class="i-icon">
                                    <use xlink:href="#icon-heart"></use>
                                </svg>
                                <div class="header__count">2</div>
                            </div>
                        </div>
                        <div class="mobile-menu__icon-right">
                            <div class="mobile-menu__icon-label">
                                Список<br>покупок
                            </div>
                        </div>
                    </div>
                    <?*/ ?>
                        <div class="mobile-menu__icon">
                            <a href="/compare/" class="mobile-menu__icon-link"></a>
                            <div class="mobile-menu__icon-left">
                                <div class="mobile-menu__icon-icon">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-comparasion"></use>
                                    </svg>
                                    <? $frame = new \Bitrix\Main\Page\FrameHelper("compare_area");
                                    $frame->begin(); ?>
                                    <div class="header__count"
                                         data-compare-count><?= CompareService::getCompareCount(); ?></div>
                                    <? $frame->beginStub(); ?>
                                    <div class="header__count" data-compare-count>0</div>
                                    <? $frame->end(); ?>
                                </div>
                            </div>
                            <div class="mobile-menu__icon-right">
                                <div class="mobile-menu__icon-label">
                                    Сравнение<br>товаров
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mobile-menu__callback">
                        <div class="header-callback">
                            <div class="header-callback__icon">
                                <svg class="i-icon">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                            </div>
                            <div class="header-callback__right">
                                <div class="header-callback__phone">
                                    <? $APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            ".default",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "COMPONENT_TEMPLATE" => ".default",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => "/include/phone.php"
                                            )
                                        ); ?>
                                    <small></small>
                                </div>
                                <div class="header-callback__link">
                                    <a href="#modal-callback" data-modal-open>Заказать обратный звонок</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <? $APPLICATION->IncludeComponent(
                        "bitrix:menu",
                        "header_mobile",
                        array(
                            "ALLOW_MULTI_SELECT" => "N",
                            "CHILD_MENU_TYPE" => "left",
                            "DELAY" => "N",
                            "MAX_LEVEL" => "2",
                            "MENU_CACHE_GET_VARS" => array(),
                            "MENU_CACHE_TIME" => "3600",
                            "MENU_CACHE_TYPE" => "N",
                            "MENU_CACHE_USE_GROUPS" => "Y",
                            "ROOT_MENU_TYPE" => "header_mobile",
                            "USE_EXT" => "Y",
                            "COMPONENT_TEMPLATE" => "header_mobile"
                        ),
                        false
                    ); ?>
                </div>
                <div class="header-mobile__middle">
                    <div class="container">
                        <div class="header-mobile__logo">
                            <a href="/">
                                <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__top">
                <div class="container">
                    <div class="header__top-wrap">
                        <div class="city-picker">
                            <a class="city-picker__label" href="#modal-city" data-modal-open="">

                                <? $frame = new \Bitrix\Main\Page\FrameHelper("area_location_two"); ?>
                                <? $frame->begin(); ?>
                                <span class="city-picker__ico">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-geolocation"></use>
                                    </svg>
                                </span>
                                <span class="city-picker__current">
                                <?= $locationName ?>
                                </span>
                                <? $frame->beginStub(); ?><span class="city-picker__current">
                                    Загрузка...
                                </span>
                                <? $frame->end(); ?>
                            </a>
                        </div>
                        <? /* Верхнюю меню - О компании, Доставка...
                            $APPLICATION->IncludeComponent(
                            "bitrix:menu",
                            "header_top",
                            array(
                                "ALLOW_MULTI_SELECT" => "N",
                                "CHILD_MENU_TYPE" => "header",
                                "DELAY" => "N",
                                "MAX_LEVEL" => "1",
                                "MENU_CACHE_GET_VARS" => array(),
                                "MENU_CACHE_TIME" => "3600",
                                "MENU_CACHE_TYPE" => "N",
                                "MENU_CACHE_USE_GROUPS" => "Y",
                                "ROOT_MENU_TYPE" => "header",
                                "USE_EXT" => "N",
                                "COMPONENT_TEMPLATE" => "header_top"
                            ),
                            false
                        ); */?>
                        <? /* Обратиться к руководителю
                        <div class="header__top-icon">
                            <a href="#modal-boss" data-modal-open class="icon-link">
                                <svg class="i-icon">
                                    <use xlink:href="#icon-mail"></use>
                                </svg>
                                <span>Обратиться к руководителю</span>
                            </a>
                        </div>
                        */ ?>
                        <div class="header__top-icon header__top-icon--active">
                            <? \Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("area_enter");
                            global $USER;
                            if ($USER->isAuthorized()) { ?>
                                <a href="/personal/" class="icon-link">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-user_3"></use>
                                    </svg>
                                    <span><?= $USER->getFullName() ?></span>
                                </a>
                            <? } else { ?>
                                <div class="icon-link">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-user_3"></use>
                                    </svg>
                                    <a href="/auth/">Вход</a> / <a href="/register/">Регистрация</a>
                                </div>
                            <? } ?>
                            <? \Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("area_enter", ""); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__middle">
                <div class="container">
                    <div class="header__middle-wrap">
                        <div class="header__logo">
                            <a href="/">
                                <span>
                                    <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.svg" alt="">
                                </span>
                                <? /* Надпись возле логотипа
                                <span class="header__logo-text">
                                    МАГАЗИН КРОВЕЛЬНЫХ <br>
                                    И ФАСАДНЫХ СТРОЙМАТЕРАЛОВ
                                </span>
                                */ ?>
                            </a>
                        </div>
                        <div class="header__links">
                            <a class="header__links__item" href="/services/calculation/">
                                <div class="header__links__ico header__links__ico--calc">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-calc"></use>
                                    </svg>
                                </div>
                                <span class="header__links__text">Бесплатный расчет</span>
                            </a>
                            <a class="header__links__item" href="/services/measurement/">
                                <div class="header__links__ico header__links__ico--roulette">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-roulette"></use>
                                    </svg>
                                </div>
                                <span class="header__links__text">Заказать замер</span>
                            </a>
                        </div>
                        <? /*
                    <div class="header__icon">
                        <a href="" class="header__icon-link"></a>
                        <div class="header__icon-icon">
                            <svg class="i-icon">
                                <use xlink:href="#icon-shoppinglist"></use>
                            </svg>
                            <div class="header__count">2</div>
                        </div>
                        <div class="header__icon-label">
                            Список<br>покупок
                        </div>
                    </div>
                    <?*/ ?>
                        <? /* Сравнение
                        <div class="header__icon header__icon--compare">
                            <a href="/compare/" class="header__icon-link"></a>
                            <div class="header__icon-icon">
                                <svg class="i-icon">
                                    <use xlink:href="#icon-comparasion"></use>
                                </svg>
                                <? $frame = new \Bitrix\Main\Page\FrameHelper("compare_area_two");
                                $frame->begin(); ?>
                                <div class="header__count"
                                     data-compare-count><?= CompareService::getCompareCount(); ?></div>
                                <? $frame->beginStub(); ?>
                                <div class="header__count" data-compare-count>0</div>
                                <? $frame->end(); ?>
                            </div>
                            <div class="header__icon-label">
                                Сравнение<br>товаров
                            </div>
                        </div>
                        */ ?>
                        <div class="header-callback">
                            <? /* Иконка телефона
                            <div class="header-callback__icon">
                                <svg class="i-icon">
                                    <use xlink:href="#icon-phone"></use>
                                </svg>
                            </div>
                            */ ?>
                            <div class="header-callback__right">
                                <div class="header-callback__phone">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/include/phone.php"
                                        )
                                    ); ?>
                                </div>
                                <div class="header-callback__info">
                                    <small>
                                        <? $APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            ".default",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "COMPONENT_TEMPLATE" => ".default",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => "/include/work-hours.php"
                                            )
                                        ); ?>
                                    </small>
                                    <div class="header-callback__link">
                                        <a href="#modal-callback" data-modal-open><span>Перезвоните мне</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header__icon header__icon--cart" data-small-cart-container>
                            <? $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.small", "", Array(
                                    "PATH_TO_BASKET" => "/personal/cart/",
                                    "PATH_TO_ORDER" => "/personal/order/make/",
                                    "SHOW_DELAY" => "Y",
                                    "SHOW_NOTAVAIL" => "Y",
                                    "SHOW_SUBSCRIBE" => "Y"
                                )
                            ); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header__bottom">
                <div class="container">
                    <div class="header__bottom-wrap">
                        <div class="main-menu">

                            <div
                                class="main-menu__item main-menu__item--catalog <?= $openDropdown ? "main-menu__item--opened" : null ?>">
                                <div class="main-menu__item-label">
                                    <a href="/catalog/" class="icon-link">
                                        <svg class="i-icon">
                                            <use xlink:href="#icon-catalog"></use>
                                        </svg>
                                        <span>Каталог</span>
                                        <svg class="i-icon">
                                            <use xlink:href="#icon-arrow"></use>
                                        </svg>
                                    </a>
                                </div>
                                <? $APPLICATION->IncludeComponent(
                                    "bitrix:catalog.section.list",
                                    "catalog-menu",
                                    Array(
                                        "VIEW_MODE" => "TEXT",
                                        "SHOW_PARENT_NAME" => "Y",
                                        "IBLOCK_TYPE" => "",
                                        "IBLOCK_ID" => IBLOCK_CATALOG,
                                        "SECTION_CODE" => "",
                                        "SECTION_URL" => "",
                                        "COUNT_ELEMENTS" => "Y",
                                        "TOP_DEPTH" => "3",
                                        "SECTION_FIELDS" => "",
                                        "SECTION_USER_FIELDS" => array("UF_BANNER_IMAGE", "UF_BANNER_IMAGE"),
                                        "ADD_SECTIONS_CHAIN" => "Y",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "CACHE_NOTES" => "",
                                        "CACHE_GROUPS" => "Y"
                                    ),
                                    false
                                ); ?>

                            </div>

                            <? $APPLICATION->IncludeComponent(
                                "bitrix:menu",
                                "header",
                                array(
                                    "ALLOW_MULTI_SELECT" => "N",
                                    "CHILD_MENU_TYPE" => "left",
                                    "DELAY" => "N",
                                    "MAX_LEVEL" => "2",
                                    "MENU_CACHE_GET_VARS" => array(),
                                    "MENU_CACHE_TIME" => "3600",
                                    "MENU_CACHE_TYPE" => "N",
                                    "MENU_CACHE_USE_GROUPS" => "Y",
                                    "ROOT_MENU_TYPE" => "top",
                                    "USE_EXT" => "Y",
                                    "COMPONENT_TEMPLATE" => "header"
                                ),
                                false
                            ); ?>

                        </div>
                        <? $APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "header",
                            array(
                                "COMPONENT_TEMPLATE" => "header",
                                "NUM_CATEGORIES" => "1",
                                "TOP_COUNT" => "5",
                                "ORDER" => "date",
                                "USE_LANGUAGE_GUESS" => "Y",
                                "CHECK_DATES" => "N",
                                "SHOW_OTHERS" => "N",
                                "PAGE" => "#SITE_DIR#catalog/index.php",
                                "SHOW_INPUT" => "Y",
                                "INPUT_ID" => "title-search-input",
                                "CONTAINER_ID" => "title-search",
                                "PRICE_CODE" => array(
                                    0 => "BASE",
                                    1 => "Спец. цена",
                                ),
                                "PRICE_VAT_INCLUDE" => "N",
                                "PREVIEW_TRUNCATE_LEN" => "",
                                "SHOW_PREVIEW" => "Y",
                                "CONVERT_CURRENCY" => "N",
                                "CATEGORY_0_TITLE" => "",
                                "CATEGORY_0" => array(
                                    0 => "iblock_1c_catalog",
                                ),
                                "PREVIEW_WIDTH" => "75",
                                "PREVIEW_HEIGHT" => "75",
                                "CATEGORY_0_iblock_1c_catalog" => array(
                                    0 => "all",
                                ),
                                "COMPOSITE_FRAME_MODE" => "A",
                                "COMPOSITE_FRAME_TYPE" => "AUTO"
                            ),
                            false
                        ); ?>
                    </div>
                </div>
            </div>

            <div class="header-fix">
                <div class="container">
                    <div class="header__middle-wrap">
                        <div class="header__logo">
                            <a href="/">
                                <img src="/local/templates/frontmaster/images/logo.png" alt="">
                            </a>
                        </div>
                        <? $APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"panel_menu", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"USE_EXT" => "N",
		"COMPONENT_TEMPLATE" => "panel_menu",
		"ROOT_MENU_TYPE" => "panel_menu",
		"CHILD_MENU_TYPE" => "",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
); ?>


                        <? $APPLICATION->IncludeComponent(
	"bitrix:search.title", 
	"panel", 
	array(
		"COMPONENT_TEMPLATE" => "panel",
		"NUM_CATEGORIES" => "0",
		"TOP_COUNT" => "0",
		"ORDER" => "date",
		"USE_LANGUAGE_GUESS" => "Y",
		"CHECK_DATES" => "N",
		"SHOW_OTHERS" => "N",
		"PAGE" => "#SITE_DIR#catalog/index.php",
		"SHOW_INPUT" => "Y",
		"INPUT_ID" => "title-search-input-panel",
		"CONTAINER_ID" => "title-search-panel",
		"PRICE_CODE" => array(
			0 => "BASE",
			1 => "Спец. цена",
		),
		"PRICE_VAT_INCLUDE" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"SHOW_PREVIEW" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CATEGORY_0_TITLE" => "",
		"CATEGORY_0" => array(
			0 => "iblock_1c_catalog",
		),
		"PREVIEW_WIDTH" => "75",
		"PREVIEW_HEIGHT" => "75",
		"CATEGORY_0_iblock_1c_catalog" => array(
			0 => "all",
		),
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
); ?>


                        <div class="header-callback">
                            <div class="header-callback__right">
                                <div class="header-callback__phone">
                                    <? $APPLICATION->IncludeComponent(
                                        "bitrix:main.include",
                                        ".default",
                                        Array(
                                            "AREA_FILE_SHOW" => "file",
                                            "AREA_FILE_SUFFIX" => "inc",
                                            "COMPONENT_TEMPLATE" => ".default",
                                            "EDIT_TEMPLATE" => "",
                                            "PATH" => "/include/phone.php"
                                        )
                                    ); ?>
                                </div>
                                <div class="header-callback__info">
                                    <small>
                                        <? $APPLICATION->IncludeComponent(
                                            "bitrix:main.include",
                                            ".default",
                                            Array(
                                                "AREA_FILE_SHOW" => "file",
                                                "AREA_FILE_SUFFIX" => "inc",
                                                "COMPONENT_TEMPLATE" => ".default",
                                                "EDIT_TEMPLATE" => "",
                                                "PATH" => "/include/work-hours.php"
                                            )
                                        ); ?>
                                    </small>
                                    <div class="header-callback__link">
                                        <a href="#modal-callback" data-modal-open><span>Перезвоните мне</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="header__icon header__icon--cart">
                            <?
                            $APPLICATION->IncludeComponent("bitrix:sale.basket.basket.line", "panel", Array(
	"HIDE_ON_BASKET_PAGES" => "Y",	// Не показывать на страницах корзины и оформления заказа
		"PATH_TO_BASKET" => SITE_DIR."personal/cart/",	// Страница корзины
		"PATH_TO_ORDER" => SITE_DIR."personal/order/make/",	// Страница оформления заказа
		"SHOW_DELAY" => "N",
		"SHOW_EMPTY_VALUES" => "Y",	// Выводить нулевые значения в пустой корзине
		"SHOW_NUM_PRODUCTS" => "Y",	// Показывать количество товаров
		"SHOW_PRICE" => "Y",
		"SHOW_SUMMARY" => "Y",
		"SHOW_TOTAL_PRICE" => "Y",	// Показывать общую сумму по товарам
	),
	false
); ?>
                        </div>
                    </div>
    </div>
            </div>

        </div>

    </header>
<main class="main <? $APPLICATION->ShowViewContent('mainClass'); ?>">

<? $curDir = $APPLICATION->getDirProperty('cur_section'); ?>
<? $curPage = $APPLICATION->getCurPage(false); ?>

<? if ($curPage != "/" && !$is404) { ?>
    <div class="main-container">
    <? if ($curDir == 'delivery') { ?>
    <div class="delivery">
    <div class="delivery__header">
<? } ?>
    <div class="container">
    <? $APPLICATION->IncludeComponent("bitrix:breadcrumb", "", Array(
            "START_FROM" => "0",
            "PATH" => "",
            "SITE_ID" => "s1"
        )
    ); ?>
    <div class="page-heading">
        <h1><? $APPLICATION->ShowTitle(false) ?></h1>
    </div>
    <? if ($curDir == 'card' || $curDir == 'contacts'){ ?>
    </div>
<? } ?>
    <? if ($curDir == 'delivery') { ?>
    </div>
    </div>
    <div class="container">
<? } ?>
    <? switch ($curDir) {
        case 'calc': ?>
            <div class="steps-block">
            <? break;
    case 'articles': ?>
        <div class="articles<? $APPLICATION->ShowViewContent("additionalClass") ?>">
        <div class="sides">
        <div class="sides__left">
            <?
            global $APPLICATION;
            $curDirectory = $APPLICATION->GetCurDir();
            ?>
            <? $APPLICATION->IncludeComponent(
                "bitrix:catalog.section.list",
                "articles",
                array(
                    'CUR_DIR' => $curDirectory,
                    "ADD_SECTIONS_CHAIN" => "N",
                    "CACHE_GROUPS" => "N",
                    "CACHE_TIME" => "36000000",
                    "CACHE_TYPE" => "A",
                    "COUNT_ELEMENTS" => "Y",
                    "IBLOCK_ID" => "5",
                    "IBLOCK_TYPE" => "articles",
                    "SECTION_CODE" => "",
                    "SECTION_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SECTION_ID" => "",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "LINE",
                    "COMPONENT_TEMPLATE" => "articles",
                    "SECTION_ID" => "",
                    "SECTION_URL" => "",
                    "SECTION_USER_FIELDS" => array(
                        0 => "",
                        1 => "",
                    ),
                    "SHOW_PARENT_NAME" => "Y",
                    "TOP_DEPTH" => "2",
                    "VIEW_MODE" => "LINE",
                    "COMPONENT_TEMPLATE" => "articles"
                ),
                false
            ); ?>
            <? $APPLICATION->IncludeComponent(
	"citfact:form", 
	"articles", 
	array(
		"AJAX" => "Y",
		"ATTACH_FIELDS" => array(
		),
		"BUILDER" => "",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => "articles",
		"DISPLAY_FIELDS" => array(
		),
		"EVENT_NAME" => "NEW_QUESTION",
		"EVENT_TEMPLATE" => "91",
		"EVENT_TYPE" => "",
		"ID" => "4",
		"REDIRECT_PATH" => "",
		"STORAGE" => "",
		"TYPE" => "HLBLOCK",
		"USE_CAPTCHA" => "N",
		"USE_CSRF" => "Y",
		"VALIDATOR" => "",
		"SHOW_AS_TEXTAREA" => array(
			0 => "UF_MESSAGE",
			1 => "",
			2 => "",
			3 => "",
		),
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
); ?>
        </div>
        <? break;
    case 'reviews': ?>
        <div class="reviews">
        <? break;
    case 'actions': ?>
        <div class="sale">
        <? break;
        case 'with-menu':
            $withMenu = true;
            $menuType = "documents";
        case 'lk': ?>
            <div class="lk">
            <div class="sides sides--l-r">
            <? global $USER;
            if ($USER->isAuthorized() || $menuType) { ?>
                <? $APPLICATION->IncludeComponent(
                    "bitrix:menu",
                    "personal_menu",
                    array(
                        "ALLOW_MULTI_SELECT" => "N",
                        "SHOW_TOPS" => $withMenu,
                        "DELAY" => "N",
                        "MAX_LEVEL" => "1",
                        "MENU_CACHE_GET_VARS" => array(),
                        "MENU_CACHE_TIME" => "3600",
                        "MENU_CACHE_TYPE" => "N",
                        "MENU_CACHE_USE_GROUPS" => "Y",
                        "ROOT_MENU_TYPE" => $menuType ?: "personal",
                        "USE_EXT" => "N",
                        "COMPONENT_TEMPLATE" => "personal_menu"
                    ),
                    false
                ); ?>
                <?
            }
            break;
    }
} ?>