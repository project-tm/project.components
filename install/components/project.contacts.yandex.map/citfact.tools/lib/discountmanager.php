<?php

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools;

class DiscountManager {

    protected $discounts = array();

    protected $basketPrice = array();

    protected static $_instance;

    public function __construct($basketPrice) {
        \CModule::includeModule("sale");

        $this->basketPrice = $basketPrice;
        $this->setTypedDiscounts();
        if (self::$_instance === null) {
            self::$_instance = $this;
        }
    }

    public static function getInstance() {
        return self::$_instance;
    }

    protected function setTypedDiscounts() {
        $arDiscounts = array();

        $dbSort = array("PRICE_FROM" => "ASC");
        $dbFilter = array("XML_ID" => "GRAPH");
        $dbSelect = array("ID", "NAME", "CONDITIONS", "ACTIONS");
        $dbDiscounts = \CSaleDiscount::GetList($dbSort, $dbFilter, false, false, $dbSelect);

        while($arDiscount = $dbDiscounts->Fetch()) {
            $conditions = unserialize($arDiscount["CONDITIONS"]);
            $actions = unserialize($arDiscount["ACTIONS"]);
            $firstAction = array_shift($actions["CHILDREN"]);

            $arDiscounts[$arDiscount["ID"]] = array(
                "percent" => $firstAction["DATA"]["Value"],
                "id" => $arDiscount["ID"]
            );

            foreach($conditions["CHILDREN"] as $children) {
                $logic = $children["DATA"]["logic"];
                $arDiscounts[$arDiscount["ID"]]["logic"][$logic] = $children["DATA"]["Value"];
            }
        }
        $this->discounts = $arDiscounts;
    }

    public function getJsonDiscounts() {
        return json_encode($this->discounts);
    }

    public function getCurrentDiscount() {
        $price = $this->getBasketPrice();
        foreach($this->discounts as $id => $discount) {
            $isCurrent = true;

            foreach($discount["logic"] as $cond => $value)
                if(!$this->inCondition($cond, $value, $price))
                    $isCurrent = false;

            if($isCurrent)
                return $id;
        }

        return false;
    }

    protected function inCondition($logic, $value, $price) {
        switch($logic) {
            case "EqGr":
                return $price >= $value;
                break;
            case "EqLs":
                return $price <= $value;
                break;
            case "Less":
                return $price < $value;
                break;
            case "Great":
                return $price > $value;
                break;
            default:
                return false;
        }
    }

    public function getTypedDiscounts() {
        return $this->discounts;
    }

    public function getBasketPrice() {
        return $this->basketPrice;
    }

}