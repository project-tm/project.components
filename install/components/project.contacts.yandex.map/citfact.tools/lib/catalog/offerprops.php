<?

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog;

class OfferProps {

    const propertyIblock = "OFFER_PROPS";

    const propertyDefaultIblock = "OFFER_PROPS_MAIN";

    const propertyStoreName = "SETTINGS_BOTTOM";

    protected $iblockId = false;

    protected $sections = array();

    protected $sectionsProps = array();

    public function __construct($iblockId, $sections) {
        \CModule::includeModule("iblock");

        $this->iblockId = $iblockId;
        $this->sections = $sections;
        $this->setCodes();
    }

    protected function setCodes() {
        $originSections = $this->getSectionsProps($this->sections);
        foreach($originSections as $id => $props) {
            $this->setSectionProps($id, $props);
        }
        if(count($this->sections) != count($this->sectionsProps))
            $this->addSectionsFromTree();

        $allSections = $this->getSectionsProps("ALL", self::propertyDefaultIblock);
        foreach($allSections as $props) {
            $this->setSectionProps("ALL", $props);
        }
    }

    protected function addSectionsFromTree() {
        $arFilter = array('IBLOCK_ID' => $this->iblockId, 'ACTIVE' => 'Y');
        $arSelect = array('ID', 'IBLOCK_SECTION_ID');

        $rsSection = \CIBlockSection::GetTreeList($arFilter, $arSelect);
        $sections = array();

        $diffSections = array_diff(
            array_keys($this->sections),
            array_keys($this->sectionsProps)
        );

        while($arSection = $rsSection->Fetch()) {
            $sections[$arSection["ID"]] = $arSection;
        }

        foreach($diffSections as $section) {
            $tree = $this->getTreeFromArray($section, $sections);
            $sectionProps = $this->getSectionsProps($tree);
            foreach($tree as $key => $id) {
                if($sectionProps[$id]) {
                    $this->setSectionProps($section, $sectionProps[$id]);
                    break;
                }
            }
        }
    }

    protected function getTreeFromArray($section, $sections) {
        $treeArr = array();
        while($sections[$section]["IBLOCK_SECTION_ID"]) {
            $section = $sections[$section]["IBLOCK_SECTION_ID"];
            $treeArr[] = $section;
        }
        return $treeArr;
    }

    protected function getSectionsProps($ids, $code = false) {
        $code = $code ?: self::propertyIblock;
        $returnSections = array();
        $dbFilter = array(
            "IBLOCK_CODE" => $code,
            "NAME" => $ids
        );
        $dbRes = \CIBlockElement::getList(array(), $dbFilter, false, false, array());

        while($arRes = $dbRes->GetNextElement()) {
            $fields = $arRes->getFields();
            $properties = $arRes->getProperties();
            $returnSections[$fields["NAME"]] = $properties[self::propertyStoreName]["~VALUE"];
        }
        return $returnSections;
    }

    protected function setSectionProps($id, $props) {
        if(empty($props))
            return;

        $codes = array();
        foreach($props as $prop) {
            $prop = unserialize($prop);
            $codes[] = $prop["CODE"];
        }
        $this->sectionsProps[$id] = $codes;
    }

    public function getDefaultCodes() {
        $props = array();
        foreach($this->sectionsProps as $section) {
            $props = array_merge($props, $section);
        }
        return $props;
    }

    public function getCodes() {
        $props = array();
        foreach($this->sectionsProps as $section) {
            $props = array_merge($props, $section);
        }
        return array_unique($props);
    }


}
?>