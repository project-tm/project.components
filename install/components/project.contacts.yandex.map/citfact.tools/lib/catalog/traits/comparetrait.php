<?
/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog\Traits;

use Citfact\Tools\Catalog\Services\CompareService;

trait CompareTrait {

    public function addToCompare() {
        return CompareService::addToCompare($this->getField("ID"));
    }

    public function removeFromCompare() {
        return CompareService::removeFromCompare($this->getField("ID"));
    }

    public function inCompare() {
        return CompareService::inCompare($this->getField("ID"));
    }
}