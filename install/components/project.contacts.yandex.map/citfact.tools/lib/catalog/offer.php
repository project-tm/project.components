<?

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog;

class Offer {

    const extendPropertyIblock = 52;

    protected $offers;

    protected $offersTree;

    protected $skuProps;

    protected $currentOffer;

    protected $offersPropsTree;

    protected $offersPropsValues;

    protected $offersPropObjects;

    protected $offersTreeIndexes;

    public function __construct($skuProps, $offers, $getDescription = false) {
        $this->offers = $offers;
        $this->skuProps = $skuProps;

        $this->extendPropertyDescription = $getDescription;

        $this->prepareOffersParams();
        $this->sortOffers();
        $this->findFirstOffer();

    }


    protected function prepareOffersParams()
    {
        $skuRows = $skuValues = $skuTree = $skuThere = array();

        foreach ($this->offers as $oKey => $arOffer) {
            if(isset($arOffer["TREE"]) && is_array($arOffer["TREE"])){
                foreach ($arOffer["TREE"] as $key => $value) {
                    $skuTree[$arOffer["ID"]][str_replace("PROP_", "", $key)] = $value;
                    if ($value > 0)
                        $skuThere[str_replace("PROP_", "", $key)][] = $value;
                }
                $this->offers[$oKey]["TREE"] =  $skuTree[$arOffer["ID"]];
            }
        }

        foreach ($this->skuProps as $arSku) {
            if (count($arSku["VALUES"]) > 0 && count($skuThere[$arSku["ID"]]) > 0) {
                $skuRows[] = $arSku["ID"];

                foreach($arSku["VALUES"] as $value)
                    if ($value["ID"] > 0 && in_array($value["ID"], $skuThere[$arSku["ID"]]))
                        $skuValues[$arSku["ID"]][] = $value;
            }
        }

        $this->offersTree = $skuTree;
        $this->offersPropsTree = $skuRows;
        $this->offersPropsValues = $skuValues;

        foreach($this->offersPropsValues as $key => $prop) {
            uasort($this->offersPropsValues[$key],  array($this, "offersNameSort"));
            $this->offersPropsValues[$key] = array_values($this->offersPropsValues[$key]);
        }

        uasort($this->offersTree, array($this, "offersTreeSort"));
        $this->offersTreeIndexes = array_keys($this->offersTree);
    }

    function sliceOffers($key, $propsObj)
    {
        $offerArray = array();
        for ($key++; $key < count($this->offersPropsTree); $key++) {
            foreach($this->offersTree as $treeId => $curTree) {
                $successAdd = true;
                foreach($propsObj as $propId => $propValue)
                    if ($curTree[$propId] != $propsObj[$propId])
                        $successAdd = false;
                if ($successAdd)
                    $offerArray[$this->offersPropsTree[$key]][] = $curTree[$this->offersPropsTree[$key]];
            }
        }
        return $offerArray;
    }

    function prepareProps($probablyProps)
    {
        foreach($probablyProps as $key => $value) {
            $propValues = $this->offersPropsValues[$key];
            foreach($propValues as $valueKey => $propValue) {
                $this->offersPropsValues[$key][$valueKey]["DISABLED"]
                    = array_search($propValues[$valueKey]["ID"], $probablyProps[$key]) === false;
            }
        }
    }

    function changeOfferProp($prop, $value, $tree = false)
    {
        $propsObj = array();
        $propIndex = array_search($prop, $this->offersPropsTree);

        foreach($this->offersPropsTree as $key => $propValue)
            if ($key < $propIndex)
                $propsObj[$this->offersPropsTree[$key]] = $this->offersPropObjects[$this->offersPropsTree[$key]];
            else if ($key == $propIndex)
                $propsObj[$this->offersPropsTree[$key]] = $value;

        $this->offersPropObjects[$prop] = $value;

        $probablyProps = $this->sliceOffers($propIndex, $propsObj);

        $this->prepareProps($probablyProps);

        foreach($probablyProps as $key => $values) {
            $sortArray = array();
            foreach($values as $value) {
                foreach($this->offersPropsValues[$key] as $propertyKey => $propertyValue) {
                    if($propertyValue["ID"] == $value)
                        $newKey = $propertyKey;
                }
                $sortArray[$newKey] = $value;
            }
            ksort($sortArray);
            $sortArray = array_values($sortArray);
            $probablyProps[$key] = $sortArray;
        }
        if(count($probablyProps) == 0)
            $this->checkOffersDiscounts($propsObj, $prop);

        $nextProbablyKey = key($probablyProps);
        $nextProbablyValue = $probablyProps[$nextProbablyKey];

        if ($nextProbablyKey !== null)
            $this->changeOfferProp($nextProbablyKey, $tree ? $tree[$nextProbablyKey] : $nextProbablyValue[0], $tree);
        else
            $this->setCurrentOffer();
    }



    protected function checkOffersDiscounts($setFields, $propKey)
    {
        $propValues = $this->offersPropsValues[$propKey];
        foreach($propValues as $valueKey => $propValue) {
            $setFields[$propKey] = $propValue["ID"];
            $curOffer = $this->getOfferByTree($setFields);
            $this->offersPropsValues[$propKey][$valueKey]["DISCOUNT"] = $curOffer
                ? $curOffer["MIN_PRICE"]["DISCOUNT_DIFF_PERCENT"]
                : 0;
        }
    }

    protected function getOfferByTree($tree)
    {
        foreach($this->offers as $offer) {
            $needOffer = true;
            foreach($tree as $propId => $propValue) {
                if($offer["TREE"][$propId] != $propValue)
                    $needOffer = false;
            }
            if($needOffer)
                return $offer;
        }
        return false;
    }

    protected function getOfferById($id)
    {
        foreach($this->offers as $offer) {
           if($offer["ID"] == $id)
               return $offer;
        }
        return false;
    }

    protected function getShortSkuInfo()
    {
        $skuProps = array();
        foreach($this->skuProps as $propIndex => $property) {
            if(in_array($property["ID"], $this->offersPropsTree))
                $skuProps[$property["ID"]] = array_intersect_key($property, array_flip(array("NAME", "CODE")));
        }
        if($this->extendPropertyDescription) {
            $descriptions = $this->getPropertyDescriptions(array_map(array($this, "getPropertyCode"), $skuProps));
            foreach($skuProps as $key => $propOne) {
                if($descriptions[$propOne["CODE"]])
                    $skuProps[$key]["DESCRIPTION"] = $descriptions[$propOne["CODE"]];
            }
        }

        return $skuProps;
    }

    protected function getPropertyDescriptions($names)
    {
        $descriptions = array();
        $dbFilter = array(
            "IBLOCK_ID" => self::extendPropertyIblock,
            "NAME" => $names
        );
        $dbSelect = array("NAME", "PREVIEW_TEXT");
        $dbElements = \CIBlockElement::getList(array(), $dbFilter, false, false, $dbSelect);

        while($arElement = $dbElements->fetch()) {
            $descriptions[$arElement["NAME"]] = $arElement["PREVIEW_TEXT"];
        }

        return $descriptions;
    }

    protected function offersValueSort($a, $b)
    {
        foreach($this->offersPropsTree as $propId) {
            $aValue = $this->findPropertyValue($propId, $a["TREE"][$propId]);
            $bValue = $this->findPropertyValue($propId, $b["TREE"][$propId]);
            return $aValue["SORT"] != $bValue["SORT"]
                ? $aValue["SORT"] > $bValue["SORT"]
                : strcasecmp($aValue["NAME"], $bValue["NAME"]);
        }
        return false;
    }


    protected function offersTreeSort($a, $b)
    {
        foreach($this->offersPropsTree as $propId) {
            $aValue = $this->findPropertyValue($propId, $a[$propId]);
            $bValue = $this->findPropertyValue($propId, $b[$propId]);

            return $aValue["SORT"] != $bValue["SORT"]
                ? $aValue["SORT"] > $bValue["SORT"]
                : strcasecmp($aValue["NAME"], $bValue["NAME"]);
        }
    }

    protected function getDefaultOffer() {
        foreach($this->offers as $offer) {
            if($offer["PROPERTIES"]["DEFAULT"]["VALUE"])
                return $offer["ID"];
        }
    }

    protected function findFirstOffer()
    {
        $tree = false;
        reset($this->offersPropsValues);

        if($activeOffer = $this->getDefaultOffer()) {
            $offer = $this->getOfferById($activeOffer);
            $firstProperty = key($offer["TREE"]);
            $firstPropertyValue = $offer["TREE"][$firstProperty];
            $tree = $offer["TREE"];
        } else {
            $firstProperty = key($this->offersPropsValues);
            $firstPropertyValue = $this->offersPropsValues[$firstProperty][0]["ID"];
        }


        $this->changeOfferProp($firstProperty, $firstPropertyValue, $tree);
    }

    public function findPropertyValue($id, $needValue)
    {
        foreach($this->offersPropsValues[$id] as $value) {
            if($value["ID"] == $needValue)
                return $value;
        }
    }

    protected function sortOffers() {
        uasort($this->offers, array($this, "offersValueSort"));
        $this->offers = array_values($this->offers);
    }

    public static function offersNameSort($var1, $var2)
    {
        return $var1["SORT"] != $var2["SORT"]
            ? $var1["SORT"] > $var2["SORT"]
            : strcasecmp($var1["NAME"], $var2["NAME"]);
    }

    protected function getPropertyCode($property)
    {
        return $property["CODE"];
    }

    protected function getOffersPropsTree()
    {
        return $this->offersPropsTree;
    }

    protected function setOffersPropsTree($tree)
    {
        $this->offersPropsTree = $tree;
    }

    protected function setCurrentOffer()
    {
        $this->currentOffer =  $this->getCurrentOffer();
    }

    public function getCurrentOffer()
    {
        return $this->getOfferByTree($this->offersPropObjects);
    }

    public function getJsParams()
    {
        return array(
            "hasOffers" => count($this->offers) > 0,
            "defaultOffer" => $this->getDefaultOffer(),
            "firstOffer" => $this->getOfferByTree($this->offersPropObjects),
            "offersTree" => $this->offersTree,
            "offersPropsDesc" => $this->getShortSkuInfo(),
            "offersPropsTree" => $this->offersPropsTree,
            "offersPropsValues" => $this->offersPropsValues,
            "offersTreeIndexes" => $this->offersTreeIndexes
        );
    }

    public function getJsOffers()
    {
        $jsOffers = array();
        $arWaterMark = Array(
            array(
                "name" => "watermark",
                "position" => "bottomleft",
                "type" => "image",
                "size" => "real",
                "file" => $_SERVER["DOCUMENT_ROOT"].'/images/wm.png',
                "fill" => "exact",
            )
        );
        $arSizes = array(
            "width" => 300,
            "height" => 300
        );
        foreach($this->offers as $key => $offer) {
            $jsOffers[$key] = array(
                'id' => $offer['ID'],
                'tree' => $offer['TREE'],
                'name' => $offer['NAME'],
                'picture' => $offer['DETAIL_PICTURE']["SRC"] ?: false,
                    /*? \CFile::resizeImageGet($offer['DETAIL_PICTURE'], $arSizes, BX_RESIZE_IMAGE_EXACT, true)
                    : false,*/
                'price' => (isset($offer['RATIO_PRICE']) ? $offer['RATIO_PRICE'] : $offer['MIN_PRICE']),
                'mq' => $offer['CATALOG_QUANTITY'],
                'ms' => $offer['~CATALOG_MEASURE_NAME'],
                'hasRelated' => !!$offer['PROPERTIES']['RELATED']['VALUE'],
                'cb' => $offer['CAN_BUY']
            );
        }
        return $jsOffers;
    }

}
?>