<?

/*
 * This file is part of the Studio Fact package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\Tools\Catalog;

use \Citfact\Tools\Catalog\Traits\CompareTrait;
use \Bitrix\Main\Loader;
use \CIBlockProperty;
use \CFile;

class Product extends \CIBlockPropertyDirectory{

    use CompareTrait;

    public static $highLoadInclude;

    protected $properties;

    protected $fields;

    protected $price;

    protected $offer = false;

    protected static $fieldsKeys = array(
        "ID",
        "NAME",
        "PREVIEW_PICTURE",
        "DETAIL_PICTURE",
        "PREVIEW_TEXT",
        "DETAIL_TEXT"
    );

    protected static $secondPriceTypes = array(
        "POLEZNAYA_PLOSHCHAD" => "*",
        "POLEZNAYA_PLOSHCHAD_1" => "*",
        "POLEZNAYA_PLOSHCHAD_PANELI" => "*",
        "POLEZNAYA_PLOSHCHAD_3" => "*",
        "POLEZNAYA_PLOSHCHAD_2" => "*",
    );

    public function __construct($element, $fromResult = false)
    {
        \CModule::includeModule("iblock");
        \CModule::includeModule("highloadblock");
        if($fromResult)
            $this->initFromResult($element);
        else
            $this->loadProduct($element);
    }

    protected function loadProduct($id)
    {
        $arFilter = array("IBLOCK_ID" => IBLOCK_CATALOG, "ID" => $id);
        $arSelect = array_merge(self::$fieldsKeys, array("PROPERTY_*"));

        $dbElement = \CIBlockElement::getList(array('PROPERTY_GROUP_SORT' => 'ASC'), $arFilter, false, false, $arSelect);
        if($arElement = $dbElement->getNextElement()) {
            $result = $arElement->getFields();
            $result["PROPERTIES"] = $arElement->getProperties();
            $this->initFromResult($result);
        }
    }

    protected function initFromResult($result)
    {
        if($props = $result["PROPERTIES"])
            $this->properties = $props;

        foreach(self::$fieldsKeys as $key)
            $this->fields[$key] = $result[$key];

        if($result["OFFERS"]) {
            $offer = new Offer($result["SKU_PROPS"], $result["OFFERS"], true);
            dump($result[$offer]);
            $this->setPrice($offer->getCurrentOffer());
            $this->setOffer($offer);
        } else {
            $this->setPrice($result);
        }
    }

    public function setOffer(Offer $offer)
    {
        $this->offer = $offer;
    }

    public function getOffer()
    {
        return $this->offer;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($result)
    {
        $this->price = array(
            "MEASURE" => $result["CATALOG_MEASURE_NAME"],
            "PRICE" => (isset($result['RATIO_PRICE']) ? $result['RATIO_PRICE'] : $result['MIN_PRICE'])
        );;
    }

    public function getProperty($name)
    {
        return $this->properties[$name];
    }

    public function getField($name)
    {
        return $this->fields[$name];
    }

    public function getSquarePrice()
    {
        foreach(self::$secondPriceTypes as $name => $type) {
            if($propValue = $this->getProperty($name)["VALUE"]){
                return str_replace(",", ".", $propValue);
            }
        }
        return false;
    }

    public function getLabels()
    {
        $arLabels = array();
        foreach($this->properties as $name => $arProperty) {
            if(preg_match("/^LABEL_(.*)/", $name) && $arProperty["VALUE"]) {
                $arLabels[] = array(
                    "name" => $arProperty["NAME"],
                    "class" => $arProperty["HINT"]
                );
            }
        }
        return $arLabels;
    }

    public function getType()
    {
        $type = $this->getProperty("TYPE");
        if(!empty($type["VALUE"])) {
            return array(
              "name" => array_shift($type["VALUE"]),
              "class" => array_shift($type["VALUE_XML_ID"]),
            );
        }

    }

    public function getPictures($sizes, $previewSizes) {
        $pictures = array();
        $detail = $this->getField("DETAIL_PICTURE");
        $otherPictures = $this->getProperty("MORE_PHOTO");

        if(is_array($detail)) {
            $detailArr = $this->getPicture($detail, $sizes);
            $detailArr["PREVIEW"] = $this->getPicture($detail, $previewSizes);
            $pictures[] = $detailArr;
        }

        if(is_array($otherPictures["VALUE"])) {
            foreach ($otherPictures["VALUE"] as $pictureId) {
                $picture = $this->getPicture($pictureId, $sizes);
                $picture["PREVIEW"] = $this->getPicture($pictureId, $previewSizes);
                $pictures[] = $picture;
            }
        }
        return $pictures;
    }

    protected function getPicture($picture, $sizes) {
        $arWaterMark = Array(
            array(
                "name" => "watermark",
                "position" => "center",
                "type" => "image",
                "size" => "big",
                "file" => $_SERVER["DOCUMENT_ROOT"].SITE_TEMPLATE_PATH."/images/vodjanoj_znak.png",
                "fill" => "exact",
            )
        );
        return array_change_key_case(
            CFile::resizeImageGet($picture, $sizes, BX_RESIZE_IMAGE_PROPORTIONAL, true, $arWaterMark),
            CASE_UPPER
        );
    }

    public static function getTreePropertyValues(&$propList, &$propNeedValues)
    {
        $result = array();
        if (!empty($propList) && is_array($propList))
        {
            foreach ($propList as $oneProperty)
            {
                $values = array();
                $valuesExist = false;
                $pictMode = ('PICT' == $oneProperty['SHOW_MODE']);
                $needValuesExist = !empty($propNeedValues[$oneProperty['ID']]) && is_array($propNeedValues[$oneProperty['ID']]);
                $filterValuesExist = ($needValuesExist && count($propNeedValues[$oneProperty['ID']]) <= 500);
                $needValues = array();
                if ($needValuesExist)
                    $needValues = array_fill_keys($propNeedValues[$oneProperty['ID']], true);
                switch($oneProperty['PROPERTY_TYPE'])
                {
                    case \Bitrix\Iblock\PropertyTable::TYPE_LIST:
                        $propEnums = CIBlockProperty::GetPropertyEnum(
                            $oneProperty['ID'],
                            array('SORT' => 'ASC', 'VALUE' => 'ASC')
                        );
                        while ($oneEnum = $propEnums->Fetch())
                        {
                            $oneEnum['ID'] = (int)$oneEnum['ID'];
                            if ($needValuesExist && !isset($needValues[$oneEnum['ID']]))
                                continue;
                            $values[$oneEnum['ID']] = array(
                                'ID' => $oneEnum['ID'],
                                'NAME' => $oneEnum['VALUE'],
                                'SORT' => (int)$oneEnum['SORT'],
                                'PICT' => false
                            );
                            $valuesExist = true;
                        }
                        $values[0] = array(
                            'ID' => 0,
                            'SORT' => PHP_INT_MAX,
                            'NA' => true,
                            'NAME' => $oneProperty['DEFAULT_VALUES']['NAME']
                        );
                        break;
                    case \Bitrix\Iblock\PropertyTable::TYPE_ELEMENT:
                        $selectFields = array('ID', 'NAME');
                        if ($pictMode)
                            $selectFields[] = 'PREVIEW_PICTURE';
                        $filterValues = (
                        $filterValuesExist
                            ? array('ID' => array_values($propNeedValues[$oneProperty['ID']]), 'IBLOCK_ID' => $oneProperty['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y')
                            : array('IBLOCK_ID' => $oneProperty['LINK_IBLOCK_ID'], 'ACTIVE' => 'Y')
                        );
                        $propEnums = CIBlockElement::GetList(
                            array('SORT' => 'ASC', 'NAME' => 'ASC'),
                            $filterValues,
                            false,
                            false,
                            $selectFields
                        );
                        while ($oneEnum = $propEnums->Fetch())
                        {
                            if ($needValuesExist && !$filterValuesExist)
                            {
                                if (!isset($needValues[$oneEnum['ID']]))
                                    continue;
                            }
                            $oneEnum['ID'] = (int)$oneEnum['ID'];
                            $values[$oneEnum['ID']] = array(
                                'ID' => $oneEnum['ID'],
                                'NAME' => $oneEnum['NAME'],
                                'SORT' => (int)$oneEnum['SORT']
                            );
                            $valuesExist = true;
                        }
                        $values[0] = array(
                            'ID' => 0,
                            'SORT' => PHP_INT_MAX,
                            'NA' => true,
                            'NAME' => $oneProperty['DEFAULT_VALUES']['NAME'],
                        );
                        break;
                    case \Bitrix\Iblock\PropertyTable::TYPE_STRING:
                        if (self::$highLoadInclude === null)
                            self::$highLoadInclude = Loader::includeModule('highloadblock');
                        if (!self::$highLoadInclude)
                            continue;
                        $xmlMap = array();
                        $sortExist = isset($oneProperty['USER_TYPE_SETTINGS']['FIELDS_MAP']['UF_SORT']);

                        $directorySelect = array('ID', 'UF_NAME', 'UF_XML_ID');
                        $directoryOrder = array();
                        if ($pictMode)
                        {
                            $directorySelect[] = 'UF_FILE';
                        }
                        if ($sortExist)
                        {
                            $directorySelect[] = 'UF_SORT';
                            $directoryOrder['UF_SORT'] = 'ASC';
                        }
                        $directoryOrder['UF_NAME'] = 'ASC';
                        $sortValue = 100;

                        /** @var Main\Entity\Base $entity */
                        $entity = $oneProperty['USER_TYPE_SETTINGS']['ENTITY'];
                        if (!($entity instanceof \Bitrix\Main\Entity\Base))
                            continue;
                        $entityDataClass = $entity->getDataClass();
                        $entityGetList = array(
                            'select' => $directorySelect,
                            'order' => $directoryOrder
                        );
                        if ($filterValuesExist)
                            $entityGetList['filter'] = array('=UF_XML_ID' => array_values($propNeedValues[$oneProperty['ID']]));
                        $propEnums = $entityDataClass::getList($entityGetList);
                        while ($oneEnum = $propEnums->fetch())
                        {
                            if ($needValuesExist && !$filterValuesExist)
                            {
                                if (!isset($needValues[$oneEnum['UF_XML_ID']]))
                                    continue;
                            }
                            $oneEnum['ID'] = (int)$oneEnum['ID'];
                            $oneEnum['UF_SORT'] = ($sortExist ? (int)$oneEnum['UF_SORT'] : $sortValue);
                            $sortValue += 100;

                            $values[$oneEnum['ID']] = array(
                                'ID' => $oneEnum['ID'],
                                'NAME' => $oneEnum['UF_NAME'],
                                'SORT' => (int)$oneEnum['UF_SORT'],
                                'XML_ID' => $oneEnum['UF_XML_ID'],
                                'COLOR' => \CFile::getFileArray($oneEnum['UF_FILE'])["SRC"] ?: "777"
                            );
                            $valuesExist = true;
                            $xmlMap[$oneEnum['UF_XML_ID']] = $oneEnum['ID'];
                        }
                        $values[0] = array(
                            'ID' => 0,
                            'SORT' => PHP_INT_MAX,
                            'NA' => true,
                            'NAME' => $oneProperty['DEFAULT_VALUES']['NAME'],
                            'XML_ID' => '',
                        );
                        if ($valuesExist)
                            $oneProperty['XML_MAP'] = $xmlMap;
                        break;
                }
                if (!$valuesExist)
                    continue;
                $oneProperty['VALUES'] = $values;
                $oneProperty['VALUES_COUNT'] = count($values);

                $result[$oneProperty['CODE']] = $oneProperty;
            }
        }
        $propList = $result;
        unset($arFilterProp);
    }

    public function getColors() {
        $colorGroup = $this->getProperty("GROUP")["VALUE"];
        $hueProperty = $this->getProperty("HUE_DETAIL");
        $hueValues = $brands = $this->getHLPropertyValues($hueProperty);
        if(!$colorGroup)
            return false;

        $arFilter = array(
            "IBLOCK_ID" => IBLOCK_CATALOG,
            "ACTIVE" => "Y",
            "PROPERTY_GROUP" => $colorGroup
        );
        $arSelect = array(
            "ID",
            "NAME",
            "DETAIL_PAGE_URL",
            "PROPERTY_HUE_DETAIL",
            "PROPERTY_MORE_PHOTO",
            "PROPERTY_TSVET_1",
            "PROPERTY_GROUP_SORT",
        );

        $dbElement = \CIBlockElement::getList(array("PROPERTY_GROUP_SORT" => "ASC"), $arFilter, false, false, $arSelect);
        $arElements = array();

        while($arElement = $dbElement->getNextElement()) {
            $result = $arElement->getFields();
            $picture = $hueValues[$result["PROPERTY_HUE_DETAIL_VALUE"]]["UF_FILE"];
            $arElements[$result["ID"]] = array(
                "id" => $result["ID"],
                "name" => $result["NAME"],
                "url" => $result["DETAIL_PAGE_URL"],
                "color_name" => $result['PROPERTY_TSVET_1_VALUE'],
                "picture" => $picture
                    ? \CFile::resizeImageGet($picture, array("width" => 50, "height" => 50),BX_RESIZE_IMAGE_PROPORTIONAL,true)["src"]
                    : false
            );
        }
        return $arElements;
    }

    public function getBrand($pictureSizes = array("width" => 200, "height" => 200)) {
        $brand = $this->getProperty("CML2_MANUFACTURER");
        if($brands = $this->getHLPropertyValues($brand)) {
            if($arBrand = $brands[$brand["VALUE"]]) {
                return array(
                    "name" => $arBrand["UF_NAME"],
                    "id" => $arBrand["UF_XML_ID"],
                    "file" => $arBrand["UF_FILE"]
                        ? \CFile::resizeImageGet($arBrand["UF_FILE"], $pictureSizes, BX_RESIZE_IMAGE_PROPORTIONAL,true, $arWaterMark)["src"]
                        : false
                );
            }
        }
        return false;
    }

    protected function getHLPropertyValues($property) {
        $tableName = $property["USER_TYPE_SETTINGS"]["TABLE_NAME"];
        if($tableName) {
            return $this->getPropertyEnum($tableName);
        }
        return false;
    }

    protected function getPropertyEnum($tableName) {
        $arValues = array();
        $obCache = new \CPHPCache();

        if( $obCache->InitCache(60, $tableName) ) {
            $arValues = $obCache->GetVars();

        } elseif($obCache->StartDataCache()) {

            $entity = $this->getPropertyTable($tableName);
            if(!$entity)
                return false;

            $hlEntity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($entity);
            $hlDataClass = $hlEntity->getDataClass();
            $hlResult = $hlDataClass::getList(array());

            while($arBrand = $hlResult->fetch()) {
                $arValues[$arBrand["UF_XML_ID"]] = $arBrand;
            }

            $obCache->EndDataCache($arValues);
        }

        return $arValues;
    }

    protected function getPropertyTable($tableName) {
        $rsData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
            'filter' => array(
                'TABLE_NAME' => $tableName
            )
        ));
        return $rsData->fetch();
    }
}

?>