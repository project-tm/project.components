<?php

/*
 * This file is part of the Studio Fact package.
 *
 * (c) Kulichkin Denis (onEXHovia) <onexhovia@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Citfact\OrderClick\Storage;

use Bitrix\Main\Request;
use Citfact\Form\Storage;
use Citfact\Form\Validator\IBlockErrorParser;

class OrderStorage
{
    /**
     * @var array
     */
    protected $errorList = array();
    /**
     * @var array
     */
    protected $delay = array();
    protected $user = array();

    /**
     * @inheritdoc
     */
    public function getErrors()
    {
        return $this->errorList;
    }

    private function delayCurrentProducts() {
        $dbBasketItems = \CSaleBasket::GetList(
            array("NAME" => "ASC","ID" => "ASC"),
            array("FUSER_ID" => \CSaleBasket::GetBasketUserID(),"LID" => SITE_ID,"ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N"),
            false,false,array()
        );
        while ($arItems = $dbBasketItems->Fetch()){
            $this->delay[] = $arItems["ID"];
            $arFields = array("CAN_BUY" => "N", "DELAY" => "Y");
            \CSaleBasket::Update($arItems["ID"], $arFields);
        }
    }

    private function returnDelayProducts() {
        foreach($this->delay as $delayItem) {
            $arFields = array("CAN_BUY" => "Y", "DELAY" => "N");
            \CSaleBasket::Update($delayItem, $arFields);
        }
    }

    protected function pregArrayKeyExists($pattern, $array) {
        $keys = array_keys($array);
        return array_shift(preg_grep($pattern, $keys));
    }

    private function addProduct($params, $request) {

        global $DB;
        $quantity = $request[$params->get("QUANTITY_FIELD")] ? $request[$params->get("QUANTITY_FIELD")] : 1;
        $currency = \CCurrency::GetBaseCurrency();
        $user = $this->user;
        if ($params->get('CLEAR_CART') == "N") {
            $this->delayCurrentProducts();
        }
        $formName = $this->pregArrayKeyExists("/FORM_/", $request);
        $element = $request[$formName][$params->get("ELEMENT_FIELD")];
        if($element  || $params->get('CLEAR_CART') == "Y") {
            if($element) {
                Add2BasketByProductID(
                    $element,
                    $quantity, array("CAN_BUY" => "Y", "DELAY" => "N"),
                    array(
                        array("NAME" => "Заказ в 1 клик", "CODE" => "orderClick", "VALUE" => "Да")
                    )
                );
            }
            $dbBasketItems = \CSaleBasket::GetList(array("NAME" => "ASC", "ID" => "ASC"), array("FUSER_ID" => \CSaleBasket::GetBasketUserID(), "ORDER_ID" => "NULL", "CAN_BUY" => "Y", "DELAY" => "N", "LID" => SITE_ID), false, false, array());
            while ($arItems = $dbBasketItems->Fetch())	{
                $arBasketItems[] = $arItems;
            }

            $productPriceSumm = 0;
            foreach ($arBasketItems as $valBasketItems) {
                $productPriceSumm = $productPriceSumm + ((int)$valBasketItems["QUANTITY"]*(int)$valBasketItems["PRICE"]);
            }

            $strOrderList = "";

            foreach ($arBasketItems as $arItem)
            {
                $measureText = (isset($arItem["MEASURE_TEXT"]) && strlen($arItem["MEASURE_TEXT"])) ? $arItem["MEASURE_TEXT"] : GetMessage("SOA_SHT");

                $strOrderList .= $arItem["NAME"]." - ".$arItem["QUANTITY"]." ".$measureText.": ".SaleFormatCurrency($arItem["PRICE"], $arItem["CURRENCY"]);
                $strOrderList .= "\n";
            }


            // Создаю заказ
            $arOrderFields = array(
                "LID" => SITE_ID,
                "PERSON_TYPE_ID" => $params->get("PERSON_TYPE"),
                "PAYED" => "N",
                "CANCELED" => "N",
                "STATUS_ID" => "N",
                "PRICE" => $productPriceSumm,
                "CURRENCY" => $currency,
                "USER_ID" => $user->getId(),
                "USER_DESCRIPTION" => "",
                "ADDITIONAL_INFO" => ""
            );

            $ORDER_ID = \CSaleOrder::Add($arOrderFields);

            if($ORDER_ID) {
                $arFields = array(
                    "ORDER_ID" => $ORDER_ID,
                    "ORDER_PROPS_ID" => 1,
                    "NAME" => "ФИО",
                    "CODE" => "FIO",
                    "VALUE" => $this->user->name
                );
                \CSaleOrderPropsValue::Add($arFields);
                $arFields = array(
                    "ORDER_ID" => $ORDER_ID,
                    "ORDER_PROPS_ID" => 3,
                    "NAME" => "Телефон",
                    "CODE" => "PHONE",
                    "VALUE" => $this->user->phone
                );
                \CSaleOrderPropsValue::Add($arFields);
            }
            \CSaleBasket::OrderBasket($ORDER_ID, \CSaleBasket::GetBasketUserID(), SITE_ID, false);

            if ($params->get('CLEAR_CART') == "N") {
                $this->returnDelayProducts();
            }

            $rsUser = \CUser::GetByID($user->getId());
            $arUser = $rsUser->Fetch();

            $arFields = Array(
                "ORDER_ID" => $ORDER_ID,
                "ORDER_DATE" => Date($DB->DateFormatToPHP(\CLang::GetDateFormat("SHORT", SITE_ID))),
                "ORDER_USER" => $arUser["NAME"],
                "PRICE" => $productPriceSumm,
                "BCC" => \COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),
                "EMAIL" => $user->getId(),
                "ORDER_LIST" => $strOrderList,
                "SALE_EMAIL" => \COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME),

            );

            $eventName = "SALE_NEW_ORDER";

            $event = new \CEvent;
            $event->Send($eventName, SITE_ID, $arFields, "N");

            return $ORDER_ID;
        }
    }

    /**
     * @inheritdoc
     */
    public function save(array $request, $builderData)
    {
        global $APPLICATION;
        $APPLICATION->restartBuffer();
        $this->user = new \Citfact\OrderClick\OrderUser($builderData);

        if (!\CModule::IncludeModule("sale")) die();

        $this->addProduct($builderData, $request);



        return false;
    }

}