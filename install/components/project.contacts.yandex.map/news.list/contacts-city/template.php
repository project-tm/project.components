<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$firstItem = $arResult["ITEMS"][$arParams['LOCATION_ID']];
?>
<div class="contacts">
    <div class="contacts__main">
        <div class="container">
            <div class="contacts__wrap">
                <div class="contacts__left">
                    <div class="city-map city-map--contacts">
                        <div class="city-map__left">
                            <div class="city-map__label">
                                Выберите город
                            </div>
                            <div class="city-map__main">
                                <div class="city-map__input">
                                    <input data-city-filter="city-filter" type="text" placeholder="Найти город">
                                </div>
                                <div class="city-map__items" data-pjax-container="city-filter">
                                    <? if (empty($arResult["ITEMS"])) {
                                        ?>
                                        <div class="contacts-info__city">Ничего не найдено</div>
                                        <?
                                    } else {
                                        foreach ($arResult["SECTIONS"] as $key => $arSection) { ?>
                                            <div class="city-map__item">
                                                <div class="city-map__item-heading">
                                                    <?= $arSection['NAME'] ?>
                                                </div>
                                                <div class="city-map__item-list">
                                                    <? foreach ($arSection['ITEMS'] as $arItem) { ?>
                                                        <div class="city-map__item-link">
                                                            <?$frame=$this->createFrame()->begin()?>
                                                            <a class="<?=($arParams['LOCATION_ID']==$arItem['id'])?'active':''?>" data-change-city="<?= $arItem['id'] ?>"
                                                               href="javascript:void(0);"><?= $arItem['name'] ?></a>
                                                            <?$frame->end();?>
                                                        </div>
                                                    <? } ?>
                                                </div>
                                            </div>
                                        <? }
                                    } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?$response = array(
                    "items" => $arResult["ITEMS"],
                    "selected" => $arParams['LOCATION_ID']
                )?>
                <div class="contacts__middle">
                    <img id="scheme-city" hidden="true" src="<?=$firstItem['scheme']['src']?>" height="562"/>
                    <?$frame=$this->createFrame()->begin()?>
                    <div data-contacts-map='<?= json_encode($response) ?>' class="contacts__map"
                         id="contacts-map">
                    </div>
                    <?$frame->end()?>
                </div>
                <div class="contacts__right">
                    <div class="contacts__info">
                        <div class="contacts-info" itemscope itemtype="http://schema.org/Organization" data-map-info>
                            <div class="contacts-info__top">
                                <div itemprop="name" hidden>Фронтмастер</div>
                                <?$frame=$this->createFrame()->begin()?>
                                <div itemprop="location" class="contacts-info__city">
                                    <?= $firstItem['name'] ?>
                                </div>
                                <?$frame->end();?>
                                <div class="contacts-info__actions">
                                    <a href="javascript:void(0);" class="contacts-info__action contacts-info__action--on-map icon-link">
                                        <svg class="i-icon">
                                            <use xlink:href="#icon-geolocation"></use>
                                        </svg>
                                        <span>На карте</span>
                                    </a>
                                    <a href="javascript:void(0);" class="contacts-info__action contacts-info__action--scheme icon-link">
                                        <svg class="i-icon">
                                            <use xlink:href="#icon-map_marker"></use>
                                        </svg>
                                        <span>Схема проезда</span>
                                    </a>
                                </div>
                            </div>
                            <div class="contacts-info__body">
                                <div class="contacts-info__item">
                                    <div class="contacts-info__item-heading">Адрес:</div>
                                    <?$frame=$this->createFrame()->begin()?>
                                    <div itemprop="address" class="contacts-info__item-info">
                                        <?= $firstItem['address'] ?>
                                    </div>
                                    <?$frame->end();?>
                                </div>
                                <div class="contacts-info__item">
                                    <div class="contacts-info__item-heading">Телефон:</div>
                                    <div  class="contacts-info__item-info">
                                        <? foreach ($firstItem['phone'] as $k => $phones) {
                                            $frame=$this->createFrame()->begin()?>
                                            <div class="call_phone_<?=$firstItem['phone_class'][$k]?>" itemprop="telephone"><?= $phones ?></div>
                                        <?$frame->end(); } ?>
                                    </div>
                                </div>
                                <div class="contacts-info__item">
                                    <div class="contacts-info__item-heading">Эл.почта:</div>
                                    <?$frame=$this->createFrame()->begin()?>
                                    <div class="contacts-info__item-info">
                                        <a itemprop="email" href="mailto:<?= $firstItem['email'] ?>"><?= $firstItem['email'] ?></a>
                                    </div>
                                    <?$frame->end();?>
                                </div>
                                <div class="contacts-info__item">
                                    <div class="contacts-info__item-heading">Режим работы:</div>
                                    <div class="contacts-info__item-info"  >
                                        <? foreach ($firstItem['graph'] as $graph) {
                                            $frame=$this->createFrame()->begin()?>
                                            <div><?= $graph ?></div>
                                        <? $frame->end(); } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="contacts-info__print">
                                <a href="javascript:void(0);" class="icon-link" onclick="goalPrintContacts();window.print();">
                                    <svg class="i-icon">
                                        <use xlink:href="#icon-print"></use>
                                    </svg>
                                    </svg>
                                    <span>Распечатать контакты</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/template" id="mapContacts">
    <div class="contacts-info__top">
        <div class="contacts-info__city">
            <%= item.name %>
        </div>
        <div class="contacts-info__actions">
            <a href="javascript:void(0);" class="contacts-info__action contacts-info__action--on-map icon-link">
                <svg class="i-icon">
                    <use xlink:href="#icon-geolocation"></use>
                </svg>
                <span>На карте</span>
            </a>
            <a href="javascript:void(0);" class="contacts-info__action contacts-info__action--scheme icon-link">
                <svg class="i-icon">
                    <use xlink:href="#icon-map_marker"></use>
                </svg>
                <span>Схема проезда</span>
            </a>
        </div>
    </div>
    <div class="contacts-info__body">
        <div class="contacts-info__item">
            <div class="contacts-info__item-heading">Адрес:</div>
            <div class="contacts-info__item-info">
                <%=item.address%>
            </div>
        </div>
        <div class="contacts-info__item">
            <div class="contacts-info__item-heading">Телефон:</div>
            <div class="contacts-info__item-info">
                <%item.phone.forEach(function(phones, key){%>
                <div class="call_phone_<%=item.phone_class[key]%>"><%=phones%></div>
                <%})%>
            </div>
        </div>
        <div class="contacts-info__item">
            <div class="contacts-info__item-heading">Эл.почта:</div>
            <div class="contacts-info__item-info">
                <a href="mailto:<%=item.email%>"><%=item.email%></a>
            </div>
        </div>
        <div class="contacts-info__item">
            <div class="contacts-info__item-heading">Режим работы:</div>
            <div class="contacts-info__item-info">
                <%item.graph.forEach(function(graphs){%>
                <div><%=graphs%></div>
                <%})%>
            </div>
        </div>
    </div>
    <div class="contacts-info__print">
        <a href="javascript:void(0);" class="icon-link" onclick="goalPrintContacts();window.print();">
            <svg class="i-icon">
                <use xlink:href="#icon-print"></use>
            </svg>
            <span>Распечатать контакты</span>
        </a>
    </div>
    </div>
</script>
