# Модуль компанентов #

- [Ajax загрузчик](#project-ajax-wrapper)

## ajax враппер для списков, форм - project:ajax.wrapper(
- загружает формы, компаненты, без вызова всего тела страницы
- можно передавать параметр PARAM
- позволяет последовательно загружать элементы с других компанентов (с пагинацией)
- фильтр по параметру, для списка
```php
$APPLICATION->IncludeComponent("project:ajax.wrapper", "callback");
$APPLICATION->IncludeComponent("project:ajax.wrapper", "onclick");
$APPLICATION->IncludeComponent("project:ajax.wrapper", "vote.main", array('PARAM' => $arResult['ID']));
```